import Vue from 'vue';
import { LANG_OPTIONS, SITE_URL } from '../common/constants';

export const LocalizedHrefMixin = Vue.extend({
  mounted() {
    this.formatLocalizedLinks();
  },
  methods: {
    formatLocalizedLinks() {
      const { localizedContainer } = this.$refs;
      const elems = this.getChildrenWithHref(localizedContainer as Element);

      if (elems.length) {
        elems
          .filter((elem) => elem.href && !elem.href.startsWith('#'))
          .forEach((elem) => {
            if (!elem.href.includes(window.location.pathname)) {
              elem.setAttribute('href', this.getLocalizedHref(elem.href));
            }
          });
      }
    },
    getChildrenWithHref(element: Element): any[] {
      let result = [];

      if (element.hasAttribute('href')) {
        result.push(element);
      }

      const { children } = element;
      for (let i = 0; i < children.length; i++) {
        const child = children[i];
        const childResult = this.getChildrenWithHref(child);
        result = [...result, ...childResult];
      }

      return result;
    },
    getUnlocalizedPath(path: string) {
      const pathLang = LANG_OPTIONS.filter((lang) => !lang.default).find(
        (lang) => path.includes(lang.path),
      );

      return pathLang ? path.replace(pathLang.path, '') : path;
    },
    getLocalizedHref(href: string) {
      const flatLang = localStorage.getItem('lang');
      const selectedLang = flatLang ? JSON.parse(flatLang) : undefined;
      const url = new URL(href);
      const unlocalizedPath = this.getUnlocalizedPath(
        `${url.pathname}${url.search}`,
      );
      let localizedPath;

      if (selectedLang) {
        // This one is only for the homepage as the method "localePath" returns a "/" when a localized page does not exists
        if (url.href.includes(SITE_URL) && url.pathname === '/') {
          localizedPath = (this as any).localePath(
            unlocalizedPath,
            selectedLang.value,
          );
          localizedPath = `${url.origin}${localizedPath}`;
        } else if (
          !url.href.includes(selectedLang.value) &&
          url.pathname !== '/'
        ) {
          localizedPath = (this as any).localePath(
            unlocalizedPath,
            selectedLang.value,
          );
          localizedPath =
            localizedPath === '/' ? url.href : `${url.origin}${localizedPath}`;
        } else {
          localizedPath = url.href;
        }
      } else {
        localizedPath = url.href;
      }

      return localizedPath;
    },
  },
});
