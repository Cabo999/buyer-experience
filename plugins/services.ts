import { Context } from '@nuxt/types';
import { WhyGitLabService } from '../services';

// eslint-disable-next-line import/no-default-export
export default ($ctx: Context, inject: any) => {
  const whyGitlabService = new WhyGitLabService($ctx);

  inject('whyGitlabService', whyGitlabService);
  // Inject more services...
};
