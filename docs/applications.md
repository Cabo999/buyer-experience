# Adding a new application to `/applications`

Edit [`content/apps.yml`](content/applications.yml) and add a new entry within
the correct categories `applications` list.

Please  add a `.png` image to [`static/nuxt-images/applications/apps`](static/nuxt-images/applications/apps),
the name of the image should be the same as the title, but with underscores instead of spaces.

Example:

```yaml
...
  - title: My new application
    content: My new application description.
    links:
      - url: https://my-new-application.io
        title: my-new-application.io
      - ...
...
```

- The image should be located in `static/nuxt-images/applications/apps/my_new_application.png`.
- The application `content` string will be truncated to 100 characters. Please do not include any HTML tags.
- The application `links` list will be truncated to 3 links.

## Adding a new category

If you **need** to create a new category, you can do so.

Please  add a new entry in `content/apps.yml`.

Example:

```yaml
...
- title: My new category
  id: my-new-category
  applications:
    - ...
    - ...
...
```

To add the menu link and assign an icon to the category in `/partners/technology-partners` page, please  add a new entry in `content/partners/technology-partners/index.yml`. Any icon from [Slippers Gallery](https://gitlab-com.gitlab.io/marketing/digital-experience/slippers-ui/?path=/story/foundations-icons--gallery) can be used assigning the name of the icon to the new category

Example:

```yaml
...
group_buttons:
  column_size: 5
  ...
  buttons:
    - text: My new category
      icon_left: slp-icon-name
      href: '#my-new-category'
      icon_right: arrow-down
...
```
