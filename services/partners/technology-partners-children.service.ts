import { PageData } from './partner-interfaces';

export function techPartnerChildrenHelper(data: any[]) {
  let pageData: PageData = {
    hero: {
      title: '',
      body: {
        text: '',
      },
      image: {
        image_url: '',
        alt: '',
      },
      primary_button: {} || null,
      secondary_button: {} || null,
    },
    copy_media: {
      container_variant: 'light-purple-100',
      container_class: 'slp-py-96',
      data: {
        block: [],
      },
    },
    body: [],
    nextStepsVariant: null,
  };

  // Take in raw data from Contentful.
  const nextSteps = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'nextSteps',
  );

  pageData.nextStepsVariant = nextSteps[0].fields.variant;

  const createHero = (data) => {
    pageData.hero = {
      title: data.title,
      body: {
        text: data.description,
      },
      image: {
        image_url: data.backgroundImage.fields.file.url,
        alt: data.backgroundImage.fields.title,
      },
      primary_button: data.primaryCta
        ? {
            url: data.primaryCta.fields.externalUrl,
            newTab: data.primaryCta.fields.openInNewTab,
            variation: data.primaryCta.fields.variation,
            text: data.primaryCta.fields.text,
            data_ga_name: data.primaryCta.fields.dataGaName,
            data_ga_location: data.primaryCta.fields.dataGaLocation
              ? data.primaryCta.fields.dataGaLocation
              : null,
          }
        : null,
      secondary_button: data.secondaryCta
        ? {
            url: data.secondaryCta.fields.externalUrl,
            newTab: data.secondaryCta.fields.openInNewTab,
            variation: data.secondaryCta.fields.variation,
            text: data.secondaryCta.fields.text,
            data_ga_name: data.secondaryCta.fields.dataGaName,
            data_ga_location: data.secondaryCta.fields.dataGaLocation
              ? data.secondaryCta.fields.dataGaLocation
              : null,
          }
        : null,
    };
  };

  const createCopyMedia = (data) => {
    pageData.copy_media.data.block.push({
      header: data.title,
      text: data.description,
      hide_horizontal_rule: true,
      image:
        data.variant == 'Side Image'
          ? {
              image_url: data.backgroundImage.fields.file.url,
            }
          : null,
      video:
        data.variant == 'Side Video'
          ? {
              video_url: data.video.fields.url,
            }
          : null,
      link_href: data.primaryCta.fields.externalUrl,
      link_text: data.primaryCta.fields.text,
      link_variant: data.primaryCta.fields.variation,
      link_newTab: data.primaryCta.fields.openInNewTab,
      link_data_ga_name: data.primaryCta.fields.dataGaName,
      link_data_ga_location: data.primaryCta.fields.dataGaLocation
        ? data.primaryCta.fields.dataGaLocation
        : null,
      secondary_link_href: data.secondaryCta.fields.externalUrl,
      secondary_link_text: data.secondaryCta.fields.text,
      secondary_link_variant: data.secondaryCta.fields.variation,
      secondary_link_newTab: data.secondaryCta.fields.openInNewTab,
      secondary_link_data_ga_name: data.secondaryCta.fields.dataGaName,
      secondary_link_data_ga_location: data.secondaryCta.fields.dataGaLocation
        ? data.secondaryCta.fields.dataGaLocation
        : null,
    });
  };

  // everything else that is NOT a sys.id of Next STeps Variant or [0], [1]
  const rawContent = data.filter((entry, index) => {
    const isNextSteps = entry.sys.contentType.sys.id === 'nextSteps';
    const isFirstEntry = index === 0;
    const isSecondEntry = index === 0;

    return !isNextSteps && !isFirstEntry && !isSecondEntry;
  });

  const createContent = (rawContent) => {
    let partnerShowcase;
    let benefits;
    let quotes = [];
    let resources;
    let copy;

    rawContent.forEach((section) => {
      if (section.sys.contentType.sys.id === 'quote') {
        quotes.push(createPullQuote(section));
      } else if (section.sys.contentType.sys.id === 'cardGroup') {
        if (section.fields.componentName === 'solutions-resource-cards') {
          resources = createResources(section);
        } else if (
          section.fields.componentName === 'partners-feature-showcase'
        ) {
          partnerShowcase = createPartnersFeatureShowcase(section);
        } else if (section.fields.componentName === 'benefits') {
          benefits = createBenefits(section);
        } else return;
      } else if (section.sys.contentType.sys.id === 'headerAndText') {
        copy = createCopy(section);
      }
    });

    pageData.body = [partnerShowcase, benefits, ...quotes, resources];
    if (copy) {
      pageData.body.unshift(copy);
    }

    return pageData;
  };

  const createPartnersFeatureShowcase = (section) => {
    return {
      name: 'partners-feature-showcase',
      data: {
        header: section.fields.header,
        text: section.fields.description,
        image_url: section.fields.image
          ? section.fields.image.fields.image.fields.file.url
          : null,
        cards: section.fields.card.map((item) => {
          return {
            header: item.fields.title,
            text: item.fields.description,
          };
        }),
      },
    };
  };

  const createCopy = (section) => {
    return {
      name: 'copy',
      data: {
        block: [
          {
            hide_horizontal_rule: true,
            header: section.fields.header,
            text: section.fields.text,
            margin_top: 32,
          },
        ],
      },
    };
  };

  const createBenefits = (section) => {
    return {
      name: 'benefits',
      data: {
        full_background: true,
        cards_per_row: 2,
        text_align: 'left',
        header: section.fields.header,
        description: section.fields.description,
        benefits: section.fields.card.map((item) => {
          return {
            no_slippers_icon: item.fields.image
              ? item.fields.image.fields.file.url
              : null,
            icon: item.fields.iconName
              ? {
                  name: item.fields.iconName,
                  alt: '',
                  variant: 'marketing',
                  hext_color: '#2F80ED',
                }
              : null,
            title: item.fields.title,
            description: item.fields.description,
            link: item.fields.button
              ? {
                  text: item.fields.button.fields.text,
                  url: item.fields.button.fields.externalUrl,
                }
              : null,
          };
        }),
      },
    };
  };

  const createPullQuote = (section) => {
    return {
      name: 'pull-quote',
      data: {
        quote: section.fields.quoteText,
        author: section.fields.author,
        title: section.fields.authorTitle ? section.fields.authorTitle : null,
        company: section.fields.authorCompany
          ? section.fields.authorCompany
          : null,
        hide_horizontal_rule: true,
      },
    };
  };

  const createResources = (section) => {
    return {
      name: 'solutions-resource-cards',
      data: {
        ...section.fields.customFields,
        title: section.fields.header,
        cards: section.fields.card.map((item) => {
          return {
            icon: {
              name: item.fields.customFields.icon.name,
              variant: 'marketing',
              alt: '',
            },
            event_type: item.fields.customFields.event_type,
            header: item.fields.title,
            link_text: item.fields.button.fields.text,
            image: item.fields.image ? item.fields.image.fields.file.url : null,
            href: item.fields.video
              ? item.fields.video.fields.url
              : item.fields.button.fields.externalUrl,
            data_ga_name: item.fields.button.fields.dataGaName
              ? item.fields.button.fields.dataGaName
              : null,
            data_ga_location: item.fields.button.fields.dataGaLocation
              ? item.fields.button.fields.dataGaLocation
              : 'resource cards',
          };
        }),
      },
    };
  };

  createHero(data[0].fields);
  createCopyMedia(data[1].fields);
  createContent(rawContent);
  return pageData;
}
