/* eslint-disable babel/camelcase */
import { getClient } from '~/plugins/contentful';
import { CONTENT_TYPES } from '~/common/content-types';
import { toKebabCase } from '@/lib/utils';

export function handleTopicFromContentful(fields) {
  const topLevelFields = {
    title: fields.seo.fields.ogTitle,
    description: fields.seo.fields.ogDescription,
    date_published: fields.datePublished,
    date_modified: fields.dateModified,
    topic_name: fields.topicName,
    icon: fields.icon,
    topics_breadcrumb: true,
  };

  const topics_header = {
    data: {
      title: fields.topicName,
      block: [
        {
          text: fields.headerText,
        },
      ],
    },
  };

  if (fields.headerCta) {
    topics_header.data.block[0].link_text = fields.headerCta.fields.text;
    topics_header.data.block[0].link_href = fields.headerCta.fields.externalUrl;
  }

  const crumbs = [
    {
      title: 'Topics',
      href: '/topics/',
      data_ga_name: 'topics',
      data_ga_location: 'breadcrumb',
    },
    { title: fields?.title },
  ];

  const side_menu = {
    anchors: {
      text: 'On this page',
      data: fields.bodyContent.map((entry) => {
        return {
          text: entry.fields.header,
          href: `#${toKebabCase(entry.fields.header)}`,
          'data-ga-title': entry.fields.header.toLowerCase(),
          'data-ga-location': 'side-navigation',
        };
      }),
    },
    content: fields?.bodyContent?.map((entry) => {
      return {
        name: 'topics-copy-block',
        data: {
          header: entry.fields.header,
          column_size: 10,
          blocks: [{ text: entry.fields.text }],
        },
      };
    }),
  };

  if (fields.sideMenuHyperlinks) {
    side_menu.hyperlinks = {
      text: fields.sideMenuTitle,
      data: fields.sideMenuHyperlinks.map((entry) => {
        return {
          text: entry.fields.text,
          href: entry.fields.externalUrl,
          variant: entry.fields.variation,
          'data-ga-name': entry.fields.text.toLowerCase(),
          'data-ga-location': 'side-navigation',
        };
      }),
    };
  }

  const components = [];

  if (fields.groupedResourceTitle) {
    components.push({
      name: 'solutions-resource-cards',
      data: {
        title: fields.groupedResourceTitle,
        column_size: 4,
        grouped: true,
        cards: fields.groupedResourceCards.map((entry) => {
          const card = {
            icon: {
              name: entry.fields.iconName,
              variant: 'marketing',
              alt: '',
            },
            event_type: entry.fields.subtitle,
            header: entry.fields?.title,
            link_text: entry.fields?.button?.fields?.text,
            href: entry.fields?.button?.fields?.externalUrl,
            data_ga_name: entry.fields.title,
            data_ga_location: 'resource cards',
          };

          if (entry.fields.image?.fields?.file?.url) {
            card.image = `${entry.fields.image?.fields?.file?.url}?h=400&fl=progressive`;
          }
          return card;
        }),
      },
    });
  }

  if (fields.resourceCardTitle) {
    components.push({
      name: 'solutions-resource-cards',
      data: {
        title: fields.resourceCardTitle,
        column_size: 4,
        cards: fields.resourceCards.map((entry) => {
          const card = {
            icon: {
              name: entry.fields.iconName,
              variant: 'marketing',
              alt: '',
            },
            event_type: entry.fields.subtitle,
            header: entry.fields?.title,
            text: entry.fields.description,
            link_text: entry.fields.button?.fields?.text,
            href: entry.fields.button?.fields?.externalUrl,
            data_ga_name: entry.fields.title,
            data_ga_location: 'resource cards',
          };
          if (entry.fields.image?.fields?.file?.url) {
            card.image = `${entry.fields.image?.fields?.file?.url}?h=400&fl=progressive`;
          }
          return card;
        }),
      },
    });
  }

  if (fields.footerCta) {
    const component = {
      name: 'topics-cta',
      data: {
        title: fields.footerCta.fields?.title,
        subtitle: fields.footerCta.fields?.subtitle,
        text: fields.footerCta.fields?.description,
        column_size: 10,
      },
    };

    if (fields.footerCta.fields?.button) {
      component.data.cta_one = {
        text: fields.footerCta.fields.button.fields.text,
        link: fields.footerCta.fields.button.fields.externalUrl,
        data_ga_name: fields.footerCta.fields.button.fields.text,
        data_ga_location: 'body',
      };
    }

    if (fields.footerCta.fields?.secondaryButton) {
      component.data.cta_two = {
        text: fields.footerCta.fields.secondaryButton.fields.text,
        link: fields.footerCta.fields.secondaryButton.fields.externalUrl,
        data_ga_name: fields.footerCta.fields.secondaryButton.fields.text,
        data_ga_location: 'body',
      };
    }
    side_menu.content.push(component);
  }

  const document = {
    ...topLevelFields,
    topics_header,
    crumbs,
    side_menu,
    components,
  };

  return document;
}

export async function fetchTopicBySlug(slug) {
  const completeSlug = `/topics/${slug}/`;

  const client = getClient();
  const entries = await client.getEntries({
    'fields.slug': completeSlug,
    content_type: CONTENT_TYPES.TOPICS,
    include: 2,
  });

  if (entries.items.length > 0) {
    return entries.items[0].fields;
  }
  return null;
}
