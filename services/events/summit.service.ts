interface SummitData {
  title: any;
  slug: string;
  date?: string;
  description: string;
  metadata?: any;
  hero?: any;
  blurb?: any;
  carousel?: any;
  faq?: any;
  questions?: any;
}

function imageBuilder(data) {
  return {
    alt: data?.altText || data?.title || '',
    src: data?.image?.fields?.file?.url || data?.file?.url || '',
    description: data?.image?.fields?.description || '',
  };
}

function heroBuilder(data) {
  const hero = {
    title: data.title || '',
    text: data.description || '',
    video: (data.video && data.video?.fields?.image?.fields.file?.url) || null,
    primary_button: data.primaryCta?.fields || {},
  };
  return hero;
}
function cardsBuilder(array) {
  return array.map((item) => imageBuilder(item.fields));
}

function arrayCleanUp(array) {
  return array.map((item) => item.fields);
}

function sectionBuilder(section) {
  const mappedData = {};

  Object.keys(section).forEach((key) => {
    if (Array.isArray(section[key])) {
      mappedData[key] = arrayCleanUp(section[key]);
    } else if (typeof section[key] === 'object') {
      mappedData[key] = section[key].fields;
    } else {
      mappedData[key] = section[key];
    }
  });
  return mappedData;
}

function faqItems(group) {
  return group.map((item) => {
    return {
      question: item?.fields.header || '',
      answer: item?.fields.text || '',
    };
  });
}

function faqHelper(data) {
  const questions =
    data.singleAccordionGroupItems && faqItems(data.singleAccordionGroupItems);

  return {
    header: data.title || null,
    id: data.id || null,
    data_inbound_analytics: data.data_inbound_analytics || null,
    background: data.background || null,
    texts: {
      show: data.expandAllCarouselItemsText || null,
      hide: data.hideAllCarouselItemsText || null,
    },
    groups: [
      {
        header: data.subtitle || '',
        questions,
      },
    ],
  };
}

export function summitHelper(data) {
  const rawData = sectionBuilder(data);
  const hero = heroBuilder(rawData.hero);
  const transformedData: SummitData = {
    title: data.title,
    slug: data.slug,
    date: data.date,
    description: data.description,
    metadata: rawData.seo && { ...rawData.seo },
    hero,
    blurb: rawData.blurb && {
      id: rawData.blurb?.headerAnchorId,
      ...rawData.blurb,
    },
    carousel: rawData.social && {
      subheader: rawData.social[0].header,
      info: rawData.social[0].text,
      cards: cardsBuilder(rawData.social[0].assets),
    },
    faq: rawData.featuredContent && faqHelper(rawData.featuredContent[0]),
    questions: rawData.footnote && {
      id: rawData.footnote[0]?.headerAnchorId,
      ...rawData.footnote[0],
    },
  };

  return transformedData;
}
