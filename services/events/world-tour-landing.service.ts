import {
  pageCleanup,
  worldTourLandingHeroBuilder,
  regionsBuilder,
  faqHelper,
  metaDataHelper,
} from '../../lib/eventsHelpers';

interface WorldTourLandingData {
  title: any;
  metadata: any;
  slug: string;
  description: string;
  hero?: any;
  regions?: any;
  form?: any;
  faq?: any;
  banner?: any;
  nextSteps?: any;
}

export function worldTourLandingHelper(data) {
  const rawData = pageCleanup(data);
  const transformedData: WorldTourLandingData = {
    title: rawData.title,
    slug: rawData.slug,
    description: rawData.description,
    hero: worldTourLandingHeroBuilder(rawData.pageContent.hero),
    regions: regionsBuilder(rawData.pageContent.regions),
    form: rawData.pageContent.form,
    faq: faqHelper(rawData.pageContent.faq),
    metadata: metaDataHelper(rawData),
    banner: rawData.pageContent.banner,
    nextSteps: rawData.pageContent.nextSteps,
  };

  return transformedData;
}
