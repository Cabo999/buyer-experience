import { Context } from '@nuxt/types';
import { getUrlFromContentfulImage } from '../common/util';
import { WhyGitlabDTO } from '../models';
import { CONTENT_TYPES } from '../common/content-types';
import { getClient } from '~/plugins/contentful.js';

export class WhyGitLabService {
  private readonly $ctx: Context;

  private readonly CONTENT_IDS = {
    hero: 'liFKTlxgNacxsfnGZaxgu',
    banner: '5ErDY0WFTqTHQo6oQc0mpT',
    categoriesBlock: '6w9WKKinT8kHr0QC56GLHI',
    badges: '37AGYcMys8b4yGGXBCB0B4',
    pricing: '5Sp2idBXJO3ajbFDXh8ShH',
  };

  constructor($context: Context) {
    this.$ctx = $context;
  }

  /**
   * This is the main method, it uses the Hybrid approach.
   * It returns data from contentful in the default locale and localized YML data from other locales
   * @param slug
   */
  getContent(slug: string): Promise<WhyGitlabDTO> {
    const isDefaultLocale =
      this.$ctx.i18n.locale === this.$ctx.i18n.defaultLocale;

    // Default locale data - Contentful
    if (isDefaultLocale) {
      return this.getContentfulData(slug);
    }

    // Localized data - YML files
    return <Promise<WhyGitlabDTO>>(
      this.$ctx.$content(this.$ctx.i18n.locale, slug).fetch()
    );
  }

  private async getContentfulData(slug: string) {
    const whyGitlabEntry = await getClient().getEntries({
      content_type: CONTENT_TYPES.PAGE,
      'fields.slug': slug,
      include: 3,
    });

    const [whyGitlab] = whyGitlabEntry.items;

    return this.transformContentfulData(whyGitlab);
  }

  /**
   * Main page mapper that converts contentful object structure to WhyGitlabDTO object structure
   * @param ctfData
   * @private
   */
  private transformContentfulData(ctfData: any): WhyGitlabDTO {
    const { pageContent, seoMetadata } = ctfData.fields;
    const { ctfHero, ctfBanner, ctfCategoriesBlock, ctfBadges, ctfPricing } =
      this.getCtfComponents(pageContent);

    const transformedData: WhyGitlabDTO = {
      title: seoMetadata[0]?.fields?.ogTitle,
      description: seoMetadata[0]?.fields?.ogDescription,
      hero: this.mapCtfHero(ctfHero),
      banner: this.mapCtfBanner(ctfBanner),
      categories_block: ctfCategoriesBlock.data,
      badges: this.mapCtfBadges(ctfBadges),
      pricing: this.mapCtfPricing(ctfPricing),
    };

    return transformedData;
  }

  /**
   * This method obtains every component from the `pageContent` array from the "PAGE" content type.
   * This method prevents the page from braking if anyone changes the order of the components in Contentful
   * @param pageContent
   * @private
   */
  private getCtfComponents(pageContent: any) {
    const result = {
      ctfHero: undefined,
      ctfBanner: undefined,
      ctfCategoriesBlock: undefined,
      ctfBadges: undefined,
      ctfPricing: undefined,
    };

    for (const pageComponent of pageContent) {
      switch (pageComponent.sys.id) {
        case this.CONTENT_IDS.hero:
          result.ctfHero = pageComponent.fields;
          break;
        case this.CONTENT_IDS.banner:
          result.ctfBanner = pageComponent.fields;
          break;
        case this.CONTENT_IDS.categoriesBlock:
          result.ctfCategoriesBlock = pageComponent.fields;
          break;
        case this.CONTENT_IDS.badges:
          result.ctfBadges = pageComponent.fields;
          break;
        case this.CONTENT_IDS.pricing:
          result.ctfPricing = pageComponent.fields;
          break;
      }
    }

    return result;
  }

  private mapCtfHero(ctfHero: any) {
    return {
      header: ctfHero?.title,
      description: ctfHero?.description,
      primary_button: {
        text: ctfHero?.primaryCta?.fields?.text,
        href: ctfHero?.primaryCta?.fields?.externalUrl,
      },
      video: {
        title: ctfHero?.video?.fields?.title,
        url: ctfHero?.video?.fields?.url,
        image:
          ctfHero?.video?.fields?.thumbnail?.fields?.image?.fields?.file?.url,
      },
    };
  }

  private mapCtfBanner(ctfBanner: any) {
    return {
      badge_text: ctfBanner?.pills[0],
      headline: ctfBanner?.title,
      blurb: ctfBanner?.description,
      button: {
        text: ctfBanner?.button?.fields.text,
        url: ctfBanner?.button?.fields.externalUrl,
        data_ga_name: ctfBanner?.button?.fields.dataGaName,
        data_ga_location: ctfBanner?.button?.fields.dataGaLocation,
      },
      features: {
        list: ctfBanner?.column2List?.map((item: string) => ({
          text: item,
        })),
      },
    };
  }

  private mapCtfBadges(ctfBadges: any) {
    return {
      header: ctfBadges.header,
      description: ctfBadges.text,
      badges: ctfBadges.assets?.map((badge) => ({
        src: getUrlFromContentfulImage(badge.fields?.image),
        alt: badge.fields?.altText,
      })),
    };
  }

  private mapCtfPricing(ctfPricing: any) {
    return {
      header: ctfPricing.title,
      footnote: ctfPricing.footnote,
      ctaText: ctfPricing.cta?.fields?.text,
      tiers: ctfPricing.tiers?.map((tier) => ({
        name: tier.fields?.title,
        benefits: tier.fields?.list,
        ctaPrimary: tier.fields?.button?.fields?.text,
        ctaSecondary: tier.fields?.secondaryButton?.fields?.text,
        purchaseUrl: tier.fields?.button?.fields?.href,
        learnMoreUrl: tier.fields?.secondaryButton?.fields?.href,
      })),
    };
  }
}
