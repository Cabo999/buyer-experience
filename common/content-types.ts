export const CONTENT_TYPES = Object.freeze({
  NEXT_STEPS: 'nextSteps',
  EVENT: 'event',
  PAGE: 'page',
  TOPICS: 'topics',
  HERO: 'eventHero',
  CARD: 'card',
  CASE_STUDY: 'caseStudy',
  CODE_SUGGESTIONS_IDE: 'codeSuggestionsIde',
});

export const NAV_CONTENT_TYPES = Object.freeze({
  NAVIGATION: 'navigation',
  FOOTER: 'footer',
});
