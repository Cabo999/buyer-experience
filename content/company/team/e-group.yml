---
  title: GitLab's Executive Group
  description: Meet GitLab's Executive Group
  main_content_block:
    header: GitLab's Executive Group
    view_more_links:
      - text: Meet our Board of Directors
        url: /company/team/board-of-directors/
        data_ga_name: "board of directors"
        data_ga_location: "body"
    headshots:
      - name: Sid Sijbrandij
        title: Co-founder, Chief Executive Officer, and GitLab Inc. Board of Directors Chair
        location: San Francisco, CA
        picture:
          url: /nuxt-images/company/team/Headshot-Sid.png
          alt: Sid Sijbrandij
        story: >-
          Sid Sijbrandij (pronounced see-brandy) is the Co-founder, Chief Executive Officer and Board Chair of GitLab Inc., the DevSecOps platform. GitLab's single application helps organizations deliver software faster and more efficiently while strengthening their security and compliance.


          Sid's career path has been anything but traditional. He spent four years building recreational submarines for U-Boat Worx and while at Ministerie van Justitie en Veiligheid he worked on the Legis project, which developed several innovative web applications to aid lawmaking. He first saw Ruby code in 2007 and loved it so much that he taught himself how to program. In 2012, as a Ruby programmer, he encountered GitLab and discovered his passion for open source. Soon after, Sid commercialized GitLab, and by 2015 he led the company through Y Combinator's Winter 2015 batch. Under his leadership, the company has grown with an estimated 30 million+ registered users from startups to global enterprises.


          Sid studied at the University of Twente in the Netherlands where he received an M.S. in Management Science. Sid was named one of the greatest minds of the pandemic by [Forbes](https://www.forbes.com/sites/elisabethbrier/2021/03/05/top-business-minds-of-the-pandemic/?sh=773356ef22ad) for spreading the gospel of remote work.
      - name: Robin Schulman
        title: Chief Legal Officer, Head of Corporate Affairs, and Corporate Secretary
        location: San Francisco, CA   
        picture:
          url: /nuxt-images/company/team/Headshot-RobinSchulman.png
          alt: Robin Schulman
        story: >-
          Robin Schulman is the Chief Legal Officer, Head of Corporate Affairs, and Corporate Secretary of GitLab Inc., the DevSecOps platform. GitLab's single application helps organizations deliver software faster and more efficiently while strengthening their security and compliance.
          

          GitLab's CLO and Corporate Secretary since 2019, Robin leverages her leadership experience scaling high growth technology companies to create a culture of compliance and set and manage the company's overall global legal, compliance, policy and privacy philosophy and strategy. Supported by a talented team of attorneys and legal professionals, Robin ensures GitLab maintains balance across the company's business activities and practices with its values and culture.  Additionally, Robin provides counsel to the GitLab Board of Directors across the entire spectrum of legal, compliance and corporate governance matters that pertain to the company. Robin is the executive sponsor of GitLab's women's and pride team member resource groups and the unofficial sponsor of our #dog Slack channel.
          

          Prior to joining GitLab, Robin oversaw global legal affairs, public policy and compliance at Couchbase, Inc. (NASDAQ: BASE) as their SVP, Chief Legal Officer and Corporate Secretary. She also established, scaled, and led New Relic Inc. 's (NYSE: NEWR) global legal and compliance organization as their General Counsel and Chief Compliance Officer from pre-IPO to profitable public company. Prior to that, Robin was Legal Counsel to Adobe Inc. (NASDAQ: ADBE) where she led the legal function for several of Adobe's Marketing and Creative Cloud products and specialized in advising high growth companies while an associate at Fenwick & West LLP, a law firm providing legal services to technology and life science companies.
          

          Robin earned a B.F.A. in Dramatic Writing and Film from New York University and a J.D. from Rutgers University School of Law - Newark. In 2017, Robin was honored  by the Silicon Valley Business Journal and the San Francisco Business Times with an award for Best Bay Area Corporate Counsel for a Public Company General Counsel. She has taught intellectual property law at Santa Clara Law School and is a board observer for a private biotech company. 
      - name: Brian Robins
        title: Chief Financial Officer
        location: Washington, D.C.
        picture:
          url: /nuxt-images/company/team/Headshot-BrianRobins.png
          alt: Brian Robins
        story: >-
          Brian Robins is the Chief Financial Officer at GitLab Inc., the DevSecOps platform. GitLab's single application helps organizations deliver software faster and more efficiently while strengthening their security and compliance.
          

          As CFO, Brian is responsible for GitLab's financial, data and business systems functions, including accounting, tax, treasury, corporate finance, IT, data science and investor relations. He helps to ensure GitLab's finance and accounting systems and processes scale and grow with the company. His team oversees GitLab's financial reporting, provides data driven decision support and offers strategic guidance to the business.
          

          Prior to GitLab, Brian served as CFO at Sisense, Cylance, AlienVault, and Verisign (NASDAQ: VRSN). As a 20-plus year veteran leading both private and public high-growth software companies, and with extensive experience with IPOs and M&As, Brian has a long, documented track record of improving financial performance, increasing productivity, and creating shareholder value. He lends this wisdom as a special advisor at Brighton Park Capital, L.P. and on the Advisory Council at ForgePoint Capital Cybersecurity.
          

          Brian holds a B.S. in Finance from Lipscomb University and an M.B.A from Vanderbilt University's Owen Graduate School of Management.
      - name: Chris Weber
        title: Chief Revenue Officer
        location: Bellevue, WA
        picture:
          url: /nuxt-images/company/team/Headshot-ChrisWeber.png
          alt: Chris Weber
        story: >-
          Chris Weber is the Chief Revenue Officer at GitLab where he leads all field operations, including sales, customer success and strategic partnerships. 


          Chris has held senior management roles for more than 25 years, leading enterprise sales strategy, building high performing teams, and managing global customer relationships across industries. Throughout his career, he has driven transformational change across enterprises and sales teams leading to exponential growth and value. 


          Prior to GitLab, Chris served as the first-ever Chief Business Officer at UiPath and held numerous leadership positions at Microsoft where he helped to transform the sales organization and built one of the world's largest cloud ecosystems of channel partners. Chris also spent time at Nokia where he led the company's re-entry into the North American market.

          
          Chris holds a bachelor's degree in business administration from the University of Mount Union.
      - name: Wendy Barnes
        title: Chief People Officer
        location: San Francisco, CA
        picture:
          url: /nuxt-images/company/team/Headshot-WendyBarnes.png
          alt: Wendy Barnes
        story: >-
          Wendy Barnes is the Chief People Officer at GitLab Inc., the DevSecOps platform. GitLab's single application helps organizations deliver software faster and more efficiently while strengthening their security and compliance. 
          

          As CPO, Wendy works closely with remote leaders to ensure the company's transparent and [values](https://about.gitlab.com/handbook/values/#credit)-driven culture scales as the company grows. She is responsible for the overall team member experience including talent brand and acquisition, operations, people technology and analytics, diversity, inclusion and belonging and total rewards.
          

          Prior to joining GitLab, Wendy has led pre-IPO and Fortune 500 companies in HR leadership roles. She served as Chief Human Resources Officer at Palo Alto Networks (NASDAQ: PANW) where she was responsible for overseeing global human resources and talent, helping the company scale from 750 to over 5,000 employees. Previously, Wendy served as Vice President, Human and Workplace Resources at eHealth (NASDAQ: EHTH), where she led a global HR organization. She has also held HR leadership roles at Netflix (NASDAQ:  NFLX) and E*TRADE (NASDAQ: ETFC, recently acquired by Morgan Stanley).
          

          Wendy earned a B.S. in Business Management from Santa Clara University and currently serves as a member of the University's Leavey School of Business Advisory Board, where she provides insight and advice to the dean, faculty, and administration on workforce trends, how to develop programs within the university that enhance business education, and how to improve industry workforce processes. In addition to her role on the board, Wendy is also a facilitator, faculty advisor, and mentor on the Women's Corporate Board Readiness Program, with a particular focus on women and underrepresented groups.
      - name: Ashley Kramer 
        title: Chief Marketing and Strategy Officer
        location: San Francisco, CA
        picture:
          url: /nuxt-images/company/team/Headshot-AshleyKramer.png
          alt: Ashley Kramer 
        story: >-
          Ashley Kramer is the Chief Marketing and Strategy Officer of GitLab Inc., the DevSecOps Platform. GitLab’s single application helps organizations deliver software faster and more efficiently while strengthening their security and compliance. As CMO and CSO, Ashley leverages her leadership from roles in marketing, product and technology to message and position GitLab as the leading DevSecOps platform through the next stage of growth. She is responsible for GitLab’s product marketing, brand awareness, communications, analyst relations, community, competitive positioning, marketing ops and revenue pipeline generation including all digital and sales development efforts. Ashley also leads the strategy for product-led growth and code contribution to the GitLab platform.
          

          Prior to joining GitLab, Ashley was CPO and CMO of Sisense and has held several post-IPO leadership roles including SVP of Product at Alteryx (NYSE: AYX) where she led the messaging, positioning and roadmap for the Alteryx Analytics Platform and Head of Cloud at Tableau where she led the effort to transform Tableau (now a Salesforce company) to a cloud-first company and ran Tableau Online, their fastest growing product. She also has held various marketing, product and engineering leadership roles at Amazon (NASDAQ: AMZN), Oracle (NYSE: ORCL) and NASA. As a former engineer, Ashley understands and can capitalize on the value of GitLab’s unique ability to solve a deep developer pain point - streamlining the development process and bringing innovative ideas to customers in a quicker and more efficient way. She approaches everything with a customer-first mindset and has a passion for solving the challenge of positioning and messaging software platforms to technical audiences.
          

          Ashley has an MSBA with a Concentration in Computer Information Science from Colorado State University and a BS in Computer Science from Old Dominion University where she also played Division I soccer. She currently is on the Board of Seeq Corporation and advises several startups.
      - name: David DeSanto
        title: Chief Product Officer
        location: Philadelphia, PA
        picture:
          url: /nuxt-images/company/team/Headshot-DavidDeSanto.png
          alt: David DeSanto
        story: >-
         David DeSanto is the Chief Product Officer at GitLab Inc., where he leads GitLab’s product division to define and execute GitLab's product vision and roadmap. David is responsible for ensuring the company builds, ships, and supports the platform that reinforces GitLab's leadership in the DevSecOps platform market.
         

         David joined GitLab in 2019 to expand GitLab’s Ultimate tier and build security into the GitLab DevOps platform. He was promoted to chief product officer in 2022. Prior to GitLab, David has held product and engineering leadership roles at Spirent Communications, NSS Labs and ICSA Labs.
         

         David holds an M.S. in Cybersecurity from New York University and a B.S. in Computer Science from Millersville University of Pennsylvania. He is a frequent speaker at major international conferences on topics including AI, DevSecOps, platform engineering, threat intelligence, and cloud security, in addition to being the co-author of Threat Forecasting.
      - name: Josh Lemos
        title: Chief Information Security Officer
        location: Seattle, WA
        picture:
          url: /nuxt-images/company/team/Headshot-JoshLemos.jpg
          alt: Josh Lemos
        story: >-
          Josh Lemos is the Chief Information Security Officer at GitLab Inc., where he brings 20 years of experience leading information security teams to his role. He is responsible for establishing and maintaining the enterprise vision, strategy, and program to ensure information assets and technologies are adequately protected, fortifying the Gitlab DevSecOps platform and ensuring the highest level of security for customers.


          A talented security practitioner and technology leader, Josh is widely recognized for his strategic vision, his ability to drive growth and innovation, and his passion for building and empowering teams. He believes in technology's potential to transform the world and the need to secure it against emerging threats. Josh has led security teams at numerous high-growth technology companies including ServiceNow, Cylance, and most recently Block (formerly known as Square). 


          Josh’s commitment to securing technologies to make a positive impact in the world has been a common thread throughout his career. He serves as a mentor to aspiring information security professionals, and is active in supporting organizations that promote diversity and inclusion in the technology industry. Josh holds a B.S. in Computer and Information Systems Security from the University of San Francisco.
 
