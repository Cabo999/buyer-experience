---
  title: A beginner’s guide to container security
  description: DevOps teams utilize containers more than ever today, but securing them is often an afterthought. Here’s what you need to know about container security.
  date_published: 2023-04-05
  date_modified: 2023-04-05
  topics_header:
    data:
      title: A beginner’s guide to container security
      block:
        - metadata:
            id_tag: beginners-guide-to-container-security
          text: |
            DevOps teams utilize containers more than ever today, but securing them is often an afterthought. Here’s what you need to know about container security.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: DevSecOps
      href: /topics/devsecops/
      data-ga-name: devsecops
      data_ga_location: breadcrumb
    - title: Beginner's Guide to Container Security
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: Container Security
          href: "#container-security"
          data_ga_name: container security
          data_ga_location: side-navigation
          variant: primary
        - text: What is container security?
          href: "#what-is-container-security"
          data_ga_name: what is container security?
          data_ga_location: side-navigation
        - text: Why container security is important?
          href: "#why-container-security-is-important"
          data_ga_name: why container security is important?
          data_ga_location: side-navigation
        - text: Many layers = many needs
          href: "#many-layers-many-needs"
          data_ga_name: many layers many needs
          data_ga_location: side-navigation
        - text: Smart secret storage
          href: "#smart-secret-storage"
          data_ga_name: smart secret storage
          data_ga_location: side-navigation
        - text: The importance of container runtime security
          href: "#the-importance-of-container-runtime-security"
          data_ga_name: the importance of container runtime security
          data_ga_location: side-navigation
        - text: Cloud native requires greater accountability
          href: "#cloud-native-requires-greater-accountability"
          data_ga_name: cloud native requires greater accountability
          data_ga_location: side-navigation
        - text: The NIST guidelines for container security
          href: "#the-nist-guidelines-for-container-security"
          data_ga_name: nist guidelines for container security
          data_ga_location: side-navigation
        - text: Learn more about container security
          href: "#learn-more-about-container-security"
          data_ga_name: learn more about container security
          data_ga_location: side-navigation
    content:
      - name: topics-copy-block
        data:
          header: Container Security
          column_size: 10
          blocks:
            - text: |
                Resource-efficient and highly portable containers are increasingly the go-to choice for modern software development. In fact, by 2023 more than 70% of organizations will be running more than two containerized applications, according to market research firm Gartner.

                But containers have their downsides, particularly when it comes to security. GitLab’s [2022 Global DevSecOps Survey](/developer-survey/){data-ga-name="developer survey" data-ga-location="body"} found that only 64% of security professionals had a security plan for containers, and many [DevOps teams](/topics/devops/){data-ga-name="devops teams" data-ga-location="body"} don’t have a plan in place for other cutting-edge software technologies, including cloud native/serverless, APIs, and microservices.

                The solution is for DevOps teams to shift left and integrate security practices into each stage of the software development process. Here’s a step-by-step look at how teams can accomplish that goal.
      - name: topics-copy-block
        data:
          header: What is container security?
          column_size: 10
          blocks:
            - text: |
                A container is a lightweight unit that contains all the components you need to run an application, such as the application code, runtime, libraries, and configurations — and container security refers to measures and practices taken with the goal of ensuring the safety and integrity of containers. Container security comprises everything from the applications inside the containers to the infrastructure they run on. Base image security and quality are critical to ensure that any derivative images come from a trusted source. Build security in your container pipeline by gathering trusted images, managing access with the use of a private registry, integrating security testing and automating deployment, and continuously defending your infrastructure. Container hardening is the process of using container scanning tools to detect possible vulnerabilities and addressing them to minimize the risk of attack.

                Robust container security reduces the risk of deploying a container that contains a security flaw or malicious code into a production environment.
      - name: topics-copy-block
        data:
          header: Why container security is important?
          column_size: 10
          blocks:
            - text: |
                Container security differs from traditional security methods due to the increased complexity and dynamism of the container environment. Simply put, there are a lot more moving pieces, and different security risks.

                While containers seem to behave like small [virtual machines (VMs)](https://searchservervirtualization.techtarget.com/definition/virtual-machine/), they actually don’t – and so require a different security strategy. Traffic between apps in a container does not cross perimeter network security, but should be monitored for malicious traffic between apps and their images. Your orchestrator can be used to set security policies for processes and resources, but a complete security strategy requires more.
      - name: topics-copy-block
        data:
          header: Many layers = many needs
          column_size: 10
          blocks:
            - text: |
                Container images define what runs in each container. Developers should make sure that images don’t contain any security vulnerabilities or compromised code, and should avoid creating extraneous images to minimize the container’s attack surface. Image validation tooling can be used to forbid untrusted images, but is often not enabled by default. Images can also be scanned after they’re built to detect dependent images that might also have vulnerabilities.

                Unlike VMs, multiple containers can run on the same operating system and an attack can happen at either level. A vulnerable host OS puts its containers at risk, and a vulnerable container can open an attack pathway to the host OS. Enforce namespace isolation to limit interaction between container and host OS kernel, and automating patching to align with vendor patch releases. The OS should also be as simple as possible, free from unnecessary components (such as apps or libraries that aren’t actually needed to run your orchestrator).

                Container orchestration coordinates and manages containers, allowing containerized applications to scale and support thousands of users. Your orchestrator may have its own out-of-the-box security features, possibly allowing you to create rules for Kubernetes to automatically enforce across all pods within the cluster. This type of basic feature is useful, but is also only a first step towards a more robust set of policies.
      - name: topics-copy-block
        data:
          header: Smart secret storage
          column_size: 10
          blocks:
            - text: |
                Containers may be spread across multiple systems and cloud providers, making access management all the more important. Secrets, which include API keys, login credentials, and tokens, should be stringently managed to ensure container access is limited to privileged users. User access can also be defined by role-based access control, allowing you to limit access to an as-needed basis.
      - name: topics-copy-block
        data:
          header: The importance of container runtime security
          column_size: 10
          blocks:
            - text: |
                Once deployed, container activity must be monitored and teams need to be able to detect and respond to any security threats. Nordcloud suggests monitoring for suspicious behaviors such as network calls, API calls, and unusual login attempts. Teams should have predefined mitigation steps for pods, and should be able to isolate the container on a different network, restart it, or stop it until the threat is identified. This can provide an additional layer of security against malware.
      - name: topics-copy-block
        data:
          header: Cloud native requires greater accountability
          column_size: 10
          blocks:
            - text: |
                Cloud providers also take on some of the work by securing the underlying hardware and basic networks used to provide cloud services, but users still share that responsibility, as well as assuming responsibility for application access, app configuration and patching, and system patching and access.
      - name: topics-copy-block
        data:
          header: The NIST guidelines for container security
          column_size: 10
          blocks:
            - text: |
                In 2017, the U.S. Department of Commerce published its [Application Container Security Guide](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-190.pdf). Although this guide is a few years old, teams just adopting containers can still benefit from the following recommendations:

                - **Tailor the organization’s operational culture and technical processes to support the new way of developing, running, and supporting applications made possible by containers:** Adopting containers might be disruptive to your existing culture and development methodologies, and your current practices might not be directly applicable in a containerized environment. Encourage, educate, and train your team to rethink how they code and operate.

                - **Use container-specific host OSs instead of general-purpose ones to reduce attack surfaces:** A container-specific host OS is a minimalist OS designed to only run containers. Using these OSs greatly reduces attack surfaces, allowing fewer opportunities for your containers to be compromised.

                - **Only group containers with the same purpose, sensitivity, and threat posture on a single host OS kernel to allow for additional in-depth defense:** Segmenting containers provides additional defense in-depth. Grouping containers in this manner makes it more difficult for an attacker to expand potential compromises to other groups. It also increases the likelihood that compromises will be detected and contained.

                - **Adopt container-specific vulnerability management tools and processes for images to prevent compromises:** Traditional tools make many assumptions that are misaligned with a containerized model, and are often unable to detect vulnerabilities within containers. Organizations should adopt tools and processes to validate and enforce compliance with secure configuration best practices for images – including centralized reporting, monitoring each image, and preventing non-compliant images from being run.

                - **Consider using hardware-based countermeasures to provide a basis for trusted computing:** Extend security practices across all tiers of the container technology by basing security on a hardware root of trust, such as the Trusted Platform Module (TPM).

                - **Use container-aware runtime defense tools:** Deploy and use a dedicated container security solution capable of monitoring the container environment and providing precise detection of anomalous and malicious activity within it.
      - name: topics-copy-block
        data:
          header: Learn more about container security
          column_size: 10
          blocks:
            - text: |
                [Learn the lingo](/blog/2020/07/30/kubernetes-terminology/){data-ga-name="kubernetes terminology" data-ga-location="body"} and get hands on.

                Dive into [DevSecOps security basics](/topics/devsecops/){data-ga-name="devsecops security" data-ga-location="body"}.

                Get started with [DevSecOps with GitLab](/solutions/security-compliance/){data-ga-name="devsecops with gitlab" data-ga-location="body"}.

