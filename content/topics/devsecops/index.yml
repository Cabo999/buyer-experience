---
  title: What is DevSecOps?
  description: Effectively turn your DevOps methodology into a DevSecOps methodology
  topic_name: DevSecOps
  icon: devsecops
  date_published: 2022-06-30
  date_modified: 2023-04-10
  topics_header:
    data:
      title: DevSecOps
      read_time: 5 min read
      block:
          - metadata:
              id_tag: what-is-devsecops
            text: |
                Can your existing DevSecOps and application security keep pace with modern development methods? Learn how next-generation software requires a new approach to app sec.
  crumbs:
      - title: Topics
        href: /topics/
        data_ga_name: topics
        data_ga_location: breadcrumb
      - title: DevSecOps
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: What is DevSecOps?
          href: "#what-is-dev-sec-ops"
          data_ga_name: what is dev sec ops
          data_ga_location: side-navigation
          variant: primary
        - text: DevSecOps vs DevOps
          href: "#dev-sec-ops-vs-dev-ops"
          data_ga_name: dev sec ops vs dev ops
          data_ga_location: side-navigation
        - text: What is application security?
          href: "#what-is-application-security"
          data_ga_name: what is application security
          data_ga_location: side-navigation
        - text: DevSecOps fundamentals
          href: "#dev-sec-ops-fundamentals"
          data_ga_name: dev sec ops fundamentals
          data_ga_location: side-navigation
        - text: Benefits of DevSecOps
          href: "#benefits-of-dev-sec-ops"
          data_ga_name: benefits of dev sec ops
          data_ga_location: side-navigation
        - text: Is DevSecOps right for your team?
          href: "#is-dev-sec-ops-right-for-your-team"
          data_ga_name: is dev sec ops right for your team
          data_ga_location: side-navigation
        - text: Creating a DevSecOps culture
          href: "#creating-a-dev-sec-ops-culture"
          data_ga_name: creating a dev sec ops culture
          data_ga_location: side-navigation
        - text: Getting started
          href: "#getting-started"
          data_ga_name: getting started
          data_ga_location: side-navigation
        - text: Assess your DevSecOps maturity
          href: "#assess-your-dev-sec-ops-maturity"
          data_ga_name: assess your dev sec ops maturity
          data_ga_location: side-navigation
    hyperlinks:
      text: ''
      data: []
    content:
      - name: topics-copy-block
        data:
          header: What is DevSecOps?
          column_size: 10
          blocks:
            - text: |
                Security has traditionally come at the end of the development lifecycle, adding cost and time when code is inevitably sent back to the developer for fixes. DevSecOps — a combination of development, security, and operations — is an approach to software development that integrates security throughout the development lifecycle.
      - name: topics-copy-block
        data:
          header: DevSecOps vs DevOps
          column_size: 10
          blocks:
            - text: DevOps [combines development and operations](https://about.gitlab.com/topics/devops/) to increase the efficiency, speed, and security of software development and delivery compared to traditional processes. A more nimble software development lifecycle results in a competitive advantage for businesses and their customers. DevOps can be best explained as people working together to conceive, build, and deliver secure software at top speed. DevOps practices enable software developers (devs) and operations (ops) teams to accelerate delivery through automation, collaboration, fast feedback, and iterative improvement.
            - text: Although the term _DevSecOps_ looks like _DevOps_ with the _Sec_ inserted in the middle, it’s more than the sum of its parts. DevSecOps is an evolution of DevOps that weaves application security practices into every stage of software development right through deployment with the use of tools and methods to protect and monitor live applications. New attack surfaces such as containers and orchestrators must be monitored and protected alongside the application itself. DevSecOps tools automate security workflows to create an adaptable process for your development and security teams, improving collaboration and breaking down silos. By embedding security into the software development lifecycle, you can consistently secure fast-moving and iterative processes, improving efficiency without sacrificing quality.
              image:
                image_url: /nuxt-images/topics/cost-savings-to-shift-left.png
                alt: cost savings
      - name: topics-copy-block
        data:
          header: What is application security?
          column_size: 10
          blocks:
            - text: Application security is[ the use of software, hardware, and procedural methods to protect applications from external threats](https://searchsoftwarequality.techtarget.com/definition/application-security). Modern approaches include _shifting left,_ or finding and fixing vulnerabilities earlier in the development process, as well as _shifting right_ to protect applications and their infrastructure-as-code in production. Securing the software development lifecycle itself is often a requirement as well.
            - text: This approach of building security into your development and operational processes effectively turns your DevOps methodology into a DevSecOps methodology. An end-to-end DevOps platform can best enable this approach.
      - name: topics-copy-block
        data:
          header: DevSecOps fundamentals
          column_size: 10
          blocks:
              - text: If you’ve read the book that was the genesis for the DevOps movement, The Phoenix Project, you understand the importance of automation, consistency, metrics, and collaboration. For DevSecOps, you are essentially applying these techniques to outfit the software factory while embedding security capabilities along the way rather than in a separate, siloed process. Both developers and security teams can find vulnerabilities, but developers are usually required to fix these flaws. It makes sense to empower them to find and fix vulnerabilities while they are still working on the code. Scanning alone isn’t enough. It’s about getting the results to the right people, at the right time, with the right context for quick action.
              - text: Fundamental DevSecOps requirements include automation and collaboration, along with policy guardrails and visibility.
              - text: |
                  ### Automation

                  GitLab’s [2022 DevSecOps Survey](https://about.gitlab.com/developer-survey/) found that a majority of DevOps teams are running static application security testing (SAST), dynamic application security testing (DAST), or other security scans regularly, but fewer than a third of developers actually get those results in their workflow. A majority of security pros say their DevOps teams are shifting left, and 47% of teams report full test automation.

                  ### Collaboration

                  A single source of truth that reports vulnerabilities and remediation provides much-needed transparency to both development and security team. It can streamline cycles, eliminate friction, and remove unnecessary translation across tools.

                  ### Policy guardrails

                  Every enterprise has a different appetite for risk. Your security policies will reflect what is right for you while the regulatory requirements to which you must adhere will also influence the policies you must apply. Hand-in-hand with automation, guardrails can ensure consistent application of your security and compliance policies.

                  ### Visibility

                  An end-to-end DevSecOps platform can give auditors a clear view into who changed what, where, when, and why from beginning to end of the software lifecyle. Leveraging a single source of truth can also ensure earlier visibility into application risks.
      - name: topics-copy-block
        data:
          header: Benefits of DevSecOps
          column_size: 10
          blocks:
              - text: |
                  ### Proactively find and fix vulnerabilities

                  Unlike traditional approaches where security is often left to the end, DevSecOps shifts security to earlier in the software development lifecycle. By reviewing, scanning, and testing code for security issues throughout the development process, teams can identify security concerns proactively and address them immediately, before additional dependencies are introduced or code is released to customers.

                  ### Release more secure software, faster

                  If security vulnerabilities aren’t detected until the end of a project, the result can be major delays as development teams scramble to address the issues at the last minute. But with a DevSecOps approach, developers can remediate vulnerabilities while they’re coding, which teaches secure code writing and reduces back and forth during security reviews. Not only does this help organizations release software faster, it ensures that their software is more secure and cost efficient.

                  ### Keep pace with modern development methods

                  Customers and business stakeholders demand software that is fast, reliable, and secure. To keep up, development teams need to leverage the latest in collaborative and security technology, including automated security testing, continuous integration and continuous delivery (CI/CD), and vulnerability patching. DevSecOps is all about improving collaboration between development, security, and operations teams to improve organizational efficiency and free up teams to focus on work that drives value for the business.
      - name: topics-copy-block
        data:
          header: Is DevSecOps right for your team?
          column_size: 10
          blocks:
              - text: |
                  The benefits of DevSecOps are clear: speed, efficiency, and collaboration. But how do you know if it’s right for your team? If your organization is facing any of the following challenges, a DevSecOps approach might be a good move:

                  1. **Development, security, and operations teams are siloed**. If development and operations are isolated from security issues, they can’t build secure software. And if security teams aren’t part of the development process, they can’t identify risks proactively. DevSecOps brings teams together to improve workflows and share ideas. Organizations might even see improved employee morale and retention.

                  2. **Long development cycles are making it difficult to meet customer or stakeholder demands**. One reason for the struggle could be security. DevSecOps implements security at every step of the development lifecycle, meaning that solid security doesn’t require the whole process to come to a halt.

                  3. **You’re migrating to the cloud (or considering it)**. Moving to the cloud often means bringing on new development processes, tools, and systems. It’s a perfect time to make processes faster and more secure — and DevSecOps could make that a lot easier.
      - name: topics-copy-block
        data:
          header: Creating a DevSecOps culture
          column_size: 10
          blocks:
              - text: |
                  Making the shift to a DevSecOps approach helps organizations address security threats in real time, but the change won’t happen overnight. The right mindset is just as important as the right toolset in making the leap. Here are five ways to prepare yourself (and your team) to embrace DevSecOps:

                  1. **Remember that security and security professionals are valuable assets, not bottlenecks or barriers**. Don’t lose sight of the wider perspective: a vulnerability that isn’t detected until later in the process is going to be much harder and more expensive to fix.

                  2. **Work in small iterations**. By delivering code in small chunks, you’ll be able to detect vulnerabilities more quickly.

                  3. **Allow everyone to contribute**. Establish a norm where everyone can comment and suggest improvement to code and processes. Encouraging everyone on the team to submit changes jumpstarts collaboration and makes it everyone’s responsibility to improve the process.

                  4. **Always be ready for an audit**. Ensure that everyone on the team understands the importance of compliance and establish norms for collecting and updating compliance information.

                  5. **Train everyone on security best practices**. Ensure that your development and operations teams are well trained in secure development by providing detailed security guidelines and hands-on training.
      - name: topics-copy-block
        data:
          header: Getting started
          column_size: 10
          blocks:
              - text: |
                  Ready to see how GitLab can help you get started with DevSecOps?

                  Our [DevSecOps Solution](/solutions/security-compliance/) page has all of the details, along with a Free Trial offer for our Ultimate tier of capabilities.
      - name: topics-cta
        data:
          subtitle: Manage your toolchain before it manages you
          text: |
              Visible, secure, and effective toolchains are difficult to come by due to the increasing number of tools teams use, and it’s placing strain on everyone involved. This study dives into the challenges, potential solutions, and key recommendations to manage this evolving complexity.
          column_size: 10
          cta_one:
            text: Read the study
            link: /resources/whitepaper-forrester-manage-your-toolchain/
            data_ga_name: Read the study
            data_ga_location: body
  components:
    - name: solutions-resource-cards
      data:
        title: Related Resources
        column_size: 4
        grouped: true
        cards:
          - icon:
              name: video
              variant: marketing
              alt: Video Icon
            event_type: "Video"
            header: Finding Bugs with Coverage Guided Fuzz Testing (DevSecOps)
            link_text: "Watch now"
            image: "/nuxt-images/topics/fuzz-testing.jpeg"
            href: https://www.youtube.com/embed/4ROYvNfRZVU
            data_ga_name: Finding Bugs with Coverage Guided Fuzz Testing (DevSecOps)
            data_ga_location: resource cards
          - icon:
              name: webcast
              variant: marketing
              alt: Webcast Icon
            event_type: "Webcast"
            header: Citizen-Centric Resiliency In Challenging Times
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_8.jpeg"
            href: https://page.gitlab.com/webcast-citizen-centric-resiliency.html
            data_ga_name: Citizen-Centric Resiliency In Challenging Times
            data_ga_location: resource cards
          - icon:
              name: report
              variant: marketing
              alt: Report Icon
            event_type: "Report"
            header: "GitLab’s 2020 Global DevSecOps Survey"
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_11.jpeg"
            href: /developer-survey/
            data_ga_name: "GitLab’s 2020 Global DevSecOps Survey"
            data_ga_location: resource cards
          - icon:
              name: report
              variant: marketing
              alt: Report Icon
            event_type: "Article"
            header: "Best Practices for a DoD DevSecOps Culture"
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_14.jpeg"
            href: https://page.gitlab.com/resources-article-RightPlatformDoD.html
            data_ga_name: "Best Practices for a DoD DevSecOps Culture"
            data_ga_location: resource cards
    - name: solutions-resource-cards
      data:
        title: Suggested Content
        column_size: 4
        cards:
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: "Blog"
            header: "Why you need static and dynamic application security testing in your development workflows"
            text: |
                  Bolster your code quality with static and dynamic application security testing. Read here on why you need SAST and DAST for your projects.
            link_text: "Learn more"
            href: /blog/2019/08/12/developer-intro-sast-dast/
            image: /nuxt-images/blogimages/autodevops.jpg
            data_ga_name: "static and dynamic application security testing"
            data_ga_location: resource cards
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: "Blog"
            header: "4 Ways developers can write secure code with GitLab"
            text: |
                GitLab Secure is not just for your security team – it’s for developers too. Learn four ways to write secure code with GitLab.
            link_text: "Learn more"
            href: /blog/2019/09/03/developers-write-secure-code-gitlab/
            image: /nuxt-images/blogimages/beginners-guide-to-ci.jpg
            data_ga_name: "4 Ways developers can write secure code with GitLab"
            data_ga_location: resource cards
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: "Blog"
            header: "5 Security testing principles every developer should know"
            text: |
                Developers are looking for guidance and standard practices as they take on more security testing responsibilities.
            link_text: "Learn more"
            href: /blog/2019/09/16/security-testing-principles-developer/
            image: /nuxt-images/blogimages/scm-ci-cr.png
            data_ga_name: "5 Security testing principles every developer should know"
            data_ga_location: resource cards
  schema_faq:
    - question: What is DevSecOps?
      answer: Security has traditionally come at the end of the development lifecycle, adding cost and time when code is inevitably sent back to the developer for fixes. It is time to shift left by embracing security earlier within the DevOps lifecycle.
    - question: What is application security?
      answer: Application security has been [defined by TechTarget](https://searchsoftwarequality.techtarget.com/definition/application-security) as the use of software, hardware, and procedural methods to protect applications from external threats. Modern approaches include shifting left to find and fix vulnerabilities earlier, while also shifting right to protect your applications and their infrastructure-as-code in production. Securing the Software Development Life Cycle (SDLC) itself is often a requirement as well.
