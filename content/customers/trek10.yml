---
  data:
    title: Trek10
    description: Focused on customizing AWS enterprise-ready solutions, Trek10 uses GitLab to foster stronger client relationships
    og_image: /nuxt-images/blogimages/paessler-case-study-image.png
    customer: Trek10
    customer_logo: /nuxt-images/logos/trek10.svg
    customer_logo_full: true
    heading:  Trek10 provides radical visibility to clients
    key_benefits:
      - label: Increased client feedback
        icon: announcement-alt
      - label: Real-time client visibility
        icon: eye-magnifying-glass
      - label: Seamless collaboration
        icon: user-collaboration
    header_image: /nuxt-images/blogimages/trek10-bg.png
    customer_industry: Technology
    customer_employee_count: 25-49
    customer_location: South Bend, Indiana, USA
    customer_solution: |
      [GitLab Premium](/pricing/premium/){data-ga-name="premium solution" data-ga-location="customers hero"}
    blurb:
      Professional services company Trek10 needed to bridge the gap between developers and clients by having everything visible in one application.
    introduction: |
      With GitLab, Trek10 is able to deliver full transparency throughout the development cycle.
    quotes:
      - text: |
           Essentially, we look at GitLab as a building block, and we just build whatever we need on top of it. Whether it's a wiki or a custom integration, GitLab helps create an engineering culture.
        author: Jared Short
        author_role: Director of Innovation
        author_company: Trek10
      - text: |
            Because GitLab hasn't broken for us in the past two and a half years, we auto update every day.
        author: Jared Short
        author_role: Director of Innovation
        author_company: Trek10
    content:
      - title: Trek10 modernizes software solutions to support clients
        description: >-
          An increasing demand for cloud native applications has left some organizations
          struggling to compete in a changing landscape. Without the teams, tools, or
          expertise to build cloud applications, organizations find themselves adrift
          in a sea of serverless solutions. How can organizations modernize their
          technology when they are not equipped with the knowledge or time to build,
          design, and support solutions?


          [Trek10](https://www.trek10.com), a professional
          services company, combines comprehensive consulting with specialized technical
          skills to build and manage serverless applications, enabling enterprises
          to migrate to cloud native architectures. Trek10 customizes solutions to
          offer clients massive scalability, heavy automation, and low operating
          costs. Clients who collaborate with Trek10 have the option to select among
          various offerings, ranging from building and operationalizing applications
          and systems on AWS to a 24/7 managed service to oversee and respond to
          incidents.


          By working with clients to determine a service offering that
          meets their specific needs and goals, Trek10 ensures that organizations
          have cutting-edge solutions to tackle business demands.

      - title: Clients require visibility and collaboration
        description: >-
          As organizations worked with Trek10 to develop customized cloud solutions,
          the need to create a transparent process became imperative as clients
          asked for updates. The team struggled with balancing the demands of
          building, designing, and meeting with clients to provide progress reports.
          Specializing in custom solutions, Trek10 encourages clients to offer
          feedback throughout the development lifecycle.


          When clients collaborated on projects to make decisions, the Trek10 team
          made adjustments, causing changes in timelines and budgets. The team tried
          several list-based todo apps to track progress, but there was always
          discrepancies and lag time between reality and the tools due to the
          overhead required from the developers. While the Trek10 team highly valued
          client collaboration to create custom solutions, the lack of visibility
          into how decisions altered original plans resulted in miscommunication
          and friction when closing out a project.


          Information was spread across different tools, causing transparency and
          collaboration issues, and Trek10 needed to bridge the gap between developers
          and clients by having everything visible in one application.

      - title: GitLab for project management fosters collaborative partnerships
        description: >-
          Jared Short, Director of Innovation, and the Trek10 team tested several tools
          in hopes of solving the transparency and collaboration conundrum. While
          some applications offered visibility, clients did not feel comfortable
          navigating through difficult UIs, so the team continued to struggle.
          Trek10 searched for a single application that brought value to both
          developers and clients. Specifically, the team looked for a way to tie
          issues more closely with code, since developers and clients were not
          contributing to a single conversation.


          Looking for an end-to-end solution, Trek10 selected GitLab to deliver
          full transparency throughout the development cycle. The team creates
          separate groups within projects and develops applications with full
          visibility to clients, paving the way for seamless collaboration after
          reviewing a repository or
          [review app environments](/stages-devops-lifecycle/review-apps/){data-ga-name="review app environments" data-ga-location="customers content"}.


          “We invite the clients and their developers, and project managers to
          collaborate with us, and we try to have as much visibility as possible,”
          explains Short. Exposing executive summaries, documentation, and spend
          reports offers clients real-time visibility into the development lifecycle.
          “With wikis, clients can easily find the information they want, helping them to
          quickly answer their own questions,” observes Short.


          The decision to use GitLab has had a significant impact on both development
          and culture, with Short remarking, “Essentially, we look at GitLab as a
          building block, and we just build whatever we need on top of it. Whether
          it's a wiki or a custom integration, GitLab helps create an engineering
          culture.”


          "At Trek10, an Amazon Web Services Advanced Consulting Partner and MSP Partner, we run many of our business and technology          operations on GitLab. This includes everything from collaboratively developing internal software, laying out and managing           clients projects, to documentation via wikis and management of deployments via GitLab CI. All of our GitLab Infrastructure          runs within our own Virtual Private Cloud in AWS (VPC). We use GitLab CI to build, test and deploy AWS infrastructure.              Whether it be containers on Fargate and ECS, or serverless solutions with Lambda and API Gateway, GItLab and its integrated         CI help us seamlessly and securely manage deployments across many AWS accounts on a daily basis for our clients as well as          our own practice," Short continued.

      - title: Increased visibility accelerates developer velocity
        description: >-
          By moving to GitLab, Trek10 increased developer velocity, improved client
          relationships, and fostered trust and transparency. Review apps have
          created a direct link between developers and clients, increasing
          communication and allowing them to seamlessly collaborate in an improved
          development flow. “The concept of review apps is powerful, because
          developers can work within their own environment, while QA can test the
          features in isolation. Showing progress to executives with review apps
          has been a valuable component in increasing developer velocity,” says Short.


          Clients have positively responded to the comprehensive wikis and issues,
          which Short says is a reassuring source of information for clients, since
          “they know that they have a place to go and immediately see everything.”
          Furthermore, the ability to collaborate directly with developers allows
          clients to maintain an active role in the development lifecycle.


          Trek10 uses GitLab as the core piece of an end-to-end solution for full
          transparency. As Short explains, “GitLab is a cohesive experience. We keep
          building off GitLab, because it works well when everybody lives in this
          little world that we created. If you adopt the philosophy and understand
          it, then it works.”


          Using GitLab as a project management application helps Trek10 focus on
          innovating solutions for its clients. “Our business runs on GitLab. It’s
          a central point of our operations,” Short proclaims. After using GitLab,
          Short emphatically believes in the application, revealing, “Because GitLab
          hasn't broken for us in the past two and a half years, we auto update every day.”


          Committed to visibility and collaboration, Trek10 uses GitLab to speed
          up development, strengthen client relationships, and create innovative
          solutions.
