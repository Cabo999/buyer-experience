title: Events
description: Where you'll find information on the GitLab happenings circuit
header:
  title: GitLab Events
  centered_by_default: true
  text: GitLab is The DevOps Platform that empowers organizations to maximize the overall return on software development. Join an event to learn how your team can deliver software faster and efficiently, while strengthening security and compliance. We'll be attending, hosting, and running numerous events this year, and cannot wait to see you there!
event_videos:
  header: Event Videos
  videos:
    - title: "Commit Virtual - Four Ways to Further FOSS"
      photourl: /nuxt-images/events/1.jpg
      video_link: https://www.youtube.com/embed/8h95PjdUyn4
      carousel_identifier:
        - 'Event Videos'

    - title: "How Delta became truly cloud native"
      photourl: /nuxt-images/events/2.jpg
      video_link: https://www.youtube-nocookie.com/embed/zV_hFcxoN8I
      carousel_identifier:
        - 'Event Videos'

    - title: "The Power of GitLab"
      photourl: /nuxt-images/events/3.jpg
      video_link: https://www.youtube-nocookie.com/embed/tIm643kyQqs
      carousel_identifier:
        - 'Event Videos'

    - title: "DevOps Culture at Porsche - A GitLab success story"
      photourl: /nuxt-images/events/4.jpg
      video_link: https://www.youtube-nocookie.com/embed/O9MdFhaosRo
      carousel_identifier:
        - 'Event Videos'

    - title: "Creating a CI/CD Pipeline with GitLab and Kubernetes in 20 Minutes"
      photourl: /nuxt-images/events/5.jpg
      video_link: https://www.youtube-nocookie.com/embed/-shvwiBwFVI
      carousel_identifier:
        - 'Event Videos'

    - title: "GitLab Product Keynote at Commit SF"
      photourl: /nuxt-images/events/6.jpg
      video_link: https://www.youtube-nocookie.com/embed/lFIk7E38Djs
      carousel_identifier:
        - 'Event Videos'

    - title: "DevSecOps & GitLab's Security Solutions"
      photourl: /nuxt-images/events/7.jpg
      video_link: https://www.youtube-nocookie.com/embed/CjX1TsCZgoQ
      carousel_identifier:
        - 'Event Videos'

    - title: "Gitlab Connect Paris 2019"
      photourl: /nuxt-images/events/8.jpg
      video_link: https://www.youtube-nocookie.com/embed/YwzpNNSdx_I
      carousel_identifier:
        - 'Event Videos'

    - title: "Verizon Connect achieves datacenter deploys in under 8 hours with GitLab"
      photourl: /nuxt-images/events/9.jpg
      video_link: https://www.youtube-nocookie.com/embed/zxMFaw5j6Zs
      carousel_identifier:
        - 'Event Videos'

    - title: "DataOps in a Cloud Native World"
      photourl: /nuxt-images/events/10.jpg
      video_link: https://www.youtube-nocookie.com/embed/PLe9sovhtGA
      carousel_identifier:
        - 'Event Videos'

events:
  - topic: Technical Demo - Delivering Amazing Digital Experiences with GitLab CI
    type: Webcast
    date_starts: October 17, 2023
    date_ends: October 17, 2023
    description: Join this technical demo to learn how Continuous Integration (CI) can help you deliver secure and reliable code to production faster and more often, without creating more risks and incidents.
    location: Virtual
    region: EMEA
    social_tags:
    event_url: https://page.gitlab.com/ci-overview-demo.html

  - topic: Technical Demo - Delivering Amazing Digital Experiences with GitLab CI
    type: Webcast
    date_starts: October 25, 2023
    date_ends: October 25, 2023
    description: Join this technical demo to learn how you can reduce the risk of non-compliance and secure your application with GitLab.
    location: Virtual
    region: EMEA
    social_tags:
    event_url: https://page.gitlab.com/managing-compliance-techdemo.html

  - topic: Creative Commons Global Summit
    type: Conference
    date_starts: October 3, 2023
    date_ends: October 6, 2023
    description: AI & the commons
    location: Mexico City, Mexico
    region: AMER
    social_tags:
    event_url: https://summit.creativecommons.org/

  - topic: Cloud Expo Europe Paris
    type: Conference
    date_starts: November 15, 2023
    date_ends: November 16, 2023
    description: Visit GitLab at Cloud Expo Europe Paris. This event brings together a community of cloud computing professionals, including decision-makers, technical experts, entrepreneurs and service providers. GitLab is at stand B60
    location: Paris Porte De Versailles, Paris
    region: EMEA
    social_tags:
    event_url: https://www.cloudexpoeurope.fr/

  - topic: Agile + DevOps East
    type: Conference
    date_starts: November 8, 2023
    date_ends: November 9, 2023
    description: Come see GitLab in Orlando, FL at Agile + DevOps East on November 8-9!
    location: Orlando, FL
    region: AMER
    social_tags:
    event_url: https://agiledevopseast.techwell.com/

  - topic: New York Digital Government Summit
    type: Conference
    date_starts: September 20, 2023
    date_ends: September 20, 2023
    description: GitLab will have a tabletop at the New York Digital Government Summit. The Digital Government Summit brings together technology focused public-sector professionals with leading industry partners to connect on innovative approaches, get inspired, and discover new technologies. Join us and let’s improve the future of government together!
    location: Albany, NY
    region: PubSec
    social_tags:
    event_url: https://events.govtech.com/new-york-digital-government-summit.html

  - topic: Technical Demo - Managing Compliance with GitLab - Enforcing separation of duties and preventing software tampering
    type: Webcast
    date_starts: September 14, 2023
    date_ends: September 14, 2023
    description: Join this technical demo to learn how you can reduce the risk of non-compliance and secure your application with GitLab.
    location: Virtual
    region: APAC
    social_tags:
    event_url: https://page.gitlab.com/managing-compliance-with-gitlab-apac.html

  - topic: AI in DevSecOps - Hands-on Workshop
    type: Workshop
    date_starts: October 10, 2023
    date_ends: October 10, 2023
    description: Are you ready to take your software development and security practices to the next level with artificial intelligence (AI)? Join our exclusive AI in DevSecOps workshop to learn how to harness the incredible potential of AI to revolutionize your workflows. This interactive workshop is designed to showcase the immediate productivity boost you can achieve by leveraging an AI-powered DevSecOps platform.
    location: Virtual
    region: AMER
    social_tags:
    event_url: https://page.gitlab.com/workshop_October10_AIWorkshop_Virtual.html

  - topic: Incorporating Security into the DevOps Lifecycle
    type: Webcast
    date_starts: September 6, 2023
    date_ends: September 6, 2023
    description: Join this session and uncover the importance of security in the software delivery lifecycle and how to incorporate it into your organization's processes.
    location: Virtual
    region: Global
    social_tags:
      - compliance
      - security
    event_url: https://page.gitlab.com/incorporating-security-into-devops-lifecycle.html

  - topic: Blackhat Europe 2023
    type: Conference
    date_starts: December 4, 2023
    date_ends: December 7, 2023
    description: Visit GitLab at stand number 249 at the Blackhat Europe 2023 in London. Learn the very latest in information security risks and trends at the Black Hat Briefings. Security experts from around the world will share ground-breaking research covering applied security, exploit development, malware, and more.
    location: Excel, London
    region: EMEA
    social_tags:
    event_url: https://blackhateurope.informatech.com/2023/?


  - topic: Accelerating Software Compliance with GitLab
    type: Webcast
    date_starts: August 30, 2023
    date_ends: August 30, 2023
    description: Join us as we discuss the growing complexities for SMB and Mid-Market companies going down the path to achieving compliance.
    location: Virtual
    region: Global
    social_tags:
      - compliance
      - security
    event_url: https://page.gitlab.com/accelerate-software-compliance-with-gitlab.html

  - topic: GitLab Connect Long Beach
    type: GitLab Connect
    date_starts: November 15, 2023
    date_ends: November 15, 2023
    description: Join us in-person to learn how the most effective application development teams leverage GitLab for DevSecOps. Featuring a 2-hour hands-on workshop and a networking lunch, this is a great opportunity to advance your GitLab skills!
    location: Long Beach, CA
    region: AMER
    social_tags:
    event_url: https://page.gitlab.com/event_november15_LongBeachHandsonWorkshop_In-Person_CA.html

  - topic: Google Cloud Next '23
    type: Conference
    date_starts: August 29, 2023
    date_ends: August 31, 2023
    description: Join GitLab at Booth no.633 and learn how GitLab and Google Cloud are partnering to deliver secure, enterprise-grade AI. Secure your spot at our pop-up meeting experience on 4th Street to check out our new suite of AI capabilities that are enhancing your experience and powering your workflows. Snag an invite to attend our sweet summer happy hour August 29th or register for our breakfast with lightning talks on key AI initiatives August 30th! 
    location: San Francisco, CA
    region: AMER
    social_tags: 
      - googlecloudnext
      - googlecloud
    event_url: https://about.gitlab.com/events/google-cloud-next/

  - topic: Technical Demo - Introduction to GitLab Security
    type: Webcast
    date_starts: August 24, 2023
    date_ends: August 24, 2023
    description: Join this Tech Demo to gain a better understanding of GitLab’s DevSecOps platform which provides many tools to enhance the security of the complete lifecycle of your application(s), including security scanners, guardrails, and vulnerability management.
    location: Virtual
    region: APAC
    social_tags:
    event_url: https://page.gitlab.com/gitlab-security-features-apac.html

  - topic: Imagine Nation ELC23 
    type: Conference
    date_starts: October 29, 2023
    date_ends: October 31, 2023
    description: Imagine Nation ELC brings together the government technology community to discuss the issues facing government and work together to develop practical solutions and innovative strategies. Visit GitLab in the partner pavilion at the event!
    location: Hershey, PA
    region: PubSec
    social_tags:
    event_url: https://web.cvent.com/event/4949d465-b72d-470f-a46c-18839336e3b3/regProcessStep1

  - topic: Georgia Digital Government Summit
    type: Conference
    date_starts: October 3, 2023
    date_ends: October 3, 2023
    description: The Digital Government Summit brings together technology focused public-sector professionals with leading industry partners to connect on innovative approaches, get inspired, and discover new technologies. Stop by and see GitLab at the event!
    location: Atlanta, GA
    region: PubSec
    social_tags:
    event_url: https://events.govtech.com/georgia-digital-government-summit.html

  - topic: COVITS 2023
    type: Conference
    date_starts: September 13, 2023
    date_ends: September 13, 2023
    description: GitLab will be exhibiting at COVITS 2023. COVITS brings together technology-focused public sector professionals with leading industry partners to connect on innovative approaches, to get inspired, and to discover new technologies. 
    location: Richmond, VA
    region: PubSec
    social_tags:
    event_url: https://events.govtech.com/covits.html

  - topic: GitLab Security + Compliance Workshop
    type: Workshop
    date_starts: September 26, 2023
    date_ends: September 26, 2023
    description: Cyber attacks have never been more in the news. From Twitter hacks to identity theft, vulnerabilities are exposing gaps in the application development process. Application security is difficult, especially when security is a separate process from your DevOps workflow. Security has traditionally been the final hurdle to conquer in the development lifecycle. Join this three hour hands-on workshop to gain a better understanding of how to successfully shift security left to find and fix security flaws during development - and to do so more easily and with greater visibility and control than typical approaches can provide.
    location: Virtual
    region: AMER
    social_tags:
    event_url: https://page.gitlab.com/workshop_september26_SecurityCompWS_Virtual_amer.html

  - topic: GitHub to GitLab Migration Workshop
    type: Workshop
    date_starts: September 28, 2023
    date_ends: September 28, 2023
    description: The GitHub to GitLab handson virtual workshop shows you the immediate value you can get out of migrating your projects to GitLab in only a few short hours. It goes beyond just migrating the project and highlights GitLabs capabilities in the Security and CI/CD space.
    location: Virtual
    region: EMEA
    social_tags:
    event_url: https://page.gitlab.com/workshop_september28_GitHubWS_EMEA.html

  - topic: Software Supply Chain Security Workshop
    type: Workshop
    date_starts: October 24, 2023
    date_ends: October 24, 2023
    description: Join us for an interactive Software Supply Chain Security virtual workshop where we will put you in the shoes of a new Security Engineering working at the Tanuki Racing start up.
    location: Virtual
    region: Emea
    social_tags:
    event_url: https://page.gitlab.com/workshop_october24_SecurityWS_EMEA.html

  - topic: DevOpsDays DC
    type: Conference
    date_starts: September 13, 2023
    date_ends: September 14, 2023
    description: GitLab will be at DevOpsDays DC which brings development, operations, QA, InfoSec, management, and leadership together to discuss the culture and tools to make better organizations and products.
    location: Washington, D.C.
    region: AMER
    social_tags:
    event_url: https://devopsdays.org/events/2023-washington-dc/welcome/

  - topic: DevSecOps - Zero to Hero Roadshow
    type: Workshop
    date_starts: August 22, 2023
    date_ends: August 22, 2023
    description: GitLab is a complete DevSecOps platform, delivered as a single application, fundamentally changing the way Development, Security, and Ops teams collaborate. Join us live for a hands-on workshop and uncover how the most effective application development teams leverage GitLab for DevSecOps. This workshop is designed for Project Management, Application Development, and Security Professionals.
    location: Utica, NY
    region: PubSec
    social_tags:
    event_url: https://page.gitlab.com/Workshop_August22_ZerotoHero_RomeNY.html

  - topic: Denver DevOps Connect with NADOG
    type: Conference
    date_starts: August 17, 2023
    date_ends: August 17, 2023
    description: Please join GitLab at DevOps Connect Denver on August 17, 2023 from 6-9pm MT. This is NADOG's quarterly, in-person gathering. These events are designed for NADOG members to connect with and learn something new from our featured speakers and to connect with and share experiences with their local peers. NADOG events are always free for qualified IT practitioners and leadership.
    location: Denver, CO
    region: AMER
    social_tags:
    event_url: https://www.eventbrite.com/e/denver-devops-connect-with-nadog-tickets-623927653407?aff=ebdsoporgprofile

  - topic: AWS Public Sector Rome
    type: Conference
    date_starts: September 28, 2023
    date_ends: September 28, 2023
    description: Please join GitLab at AWS PubSec Rome. This is a free event and a great way to meet with industry peers and participate in talks, technical and business discussions.
    location: La Nuvola, Rome
    region: EMEA
    social_tags:
    event_url: https://aws.amazon.com/it/events/summits/public-sector-symposium-rome/

  - topic: GitLab CI/CD Virtual Workshop
    type: Workshop
    date_starts: August 23, 2023
    date_ends: August 23, 2023
    description: GitLab is a complete DevOps platform, delivered as a single application, fundamentally changing the way Development, Security, and Ops teams collaborate. You might have had the chance to take it for a test drive, but even our most advanced users know there’s still a lot to learn to push better and faster automation throughout your DevOps lifecycle! We’re inviting you to a deep-dive workshop on Advanced GitLab CI/CD, including everything you need to enable you to take your automation game to the next level, and provide thought leadership within your organization.
    location: Virtual
    region: AMER
    social_tags:
    event_url: https://page.gitlab.com/workshop_August23_CICDWS_Virtual.html

  - topic: Annual CISO Summit Half Moon Bay
    type: Conference
    date_starts: December 5, 2023
    date_ends: December 5, 2023
    description: GitLab is proud to sponsor the Half Moon Bay CISO Summit on December 5th. This is an invitation only event. Register to request to attend and meet us there.
    location: Half Moon Bay, CA
    region: AMER
    social_tags:
    event_url: https://www.gbiimpact.com/summits/half-moon-bay-summit-ciso

  - topic: Irvine DevOps Connect with NADOG
    type: Conference
    date_starts: September 7, 2023
    date_ends: September 7, 2023
    description: Please join GitLab at Irvine DevOps Connect with NADOG on September 7 from 6 - 8pm PDT. This is NADOG's quarterly in-person gathering. Connect events are designed for our members to connect with and learn something new from our featured speakers, to connect with and share their experiences and insights with their peers. NADOG events are always free for qualified IT practitioners and leadership.
    location: Irvine, CA
    region: AMER
    social_tags:
    event_url: https://www.eventbrite.com/e/irvine-devops-connect-with-nadog-tickets-541960948787?aff=ebdshpsearchautocomplete

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: August 17, 2023
    date_ends: August 17, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: Melbourne, AU
    region: APAC
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: AWS Summit São Paulo
    type: Conference
    date_starts: August 3, 2023
    date_ends: August 3, 2023
    description: Junte-se a nós presencialmente no AWS Summit São Paulo. Assista ao Keynote, caminhe pela Expo e converse com os especialistas. Veja como seus colegas e concorrentes estão usando a nuvem a seu favor e descubra todas as formas que você pode utilizar a AWS para iniciar, crescer ou impulsionar seus negócios e carreira para o próximo nível. O AWS Summit São Paulo é um evento gratuito.
    location: São Paulo, Brazil
    region: LATAM
    social_tags:
    event_url: https://register.awsevents.com/ereg/index.php?eventid=750069&categoryid=4907060&reference=61ff38a5-c463-45ae-a8ca-d47aa85c0ab8&sc_channel=el

  - topic: Phoenix DevOps Connect with NADOG
    type: Conference
    date_starts: September 19, 2023
    date_ends: September 19, 2023
    description: Please join GitLab at DevOps Connect Phoenix on  September 19, 2023 from 6 - 8pm MST. This is NADOG's quarterly in-person gathering. Connect events are designed for NADOG members to connect with and learn something new from our featured speakers, and to connect with and share experiences with their local peers. NADOG events are always free for qualified IT practitioners and leadership.
    location: Scottsdale, AZ
    region: AMER
    social_tags:
    event_url: https://www.eventbrite.com/e/phoenix-devops-connect-with-nadog-tickets-546611197807?aff=ebdshpsearchautocomplete

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: September 12, 2023
    date_ends: September 12, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: London, UK
    region: EMEA
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: ELC Annual SF Conference
    type: Conference
    date_starts: August 30, 2023
    date_ends: August 31, 2023
    description: GitLab is a silver sponsor for ELC Annual 2023, an in-person Engineering Leadership Conference held in San Francisco from August 30-31 at Fort Mason. If you are interested in a 20% event registration discount, please contact lrom@gitlab.com.
    location: San Francisco, CA
    region: AMER
    social_tags:
    event_url: https://sfelc.com/annual2023

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: September 19, 2023
    date_ends: September 19, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: Mountain View, CA
    region: AMER
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: September 21, 2023
    date_ends: September 21, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: Irvine, CA
    region: AMER
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: October 5, 2023
    date_ends: October 5, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: Dallas, TX
    region: AMER
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: October 11, 2023
    date_ends: October 11, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: New York, NY
    region: AMER
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: October 17, 2023
    date_ends: October 17, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: Paris, FR
    region: EMEA
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: October 19, 2023
    date_ends: October 19, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: Berlin, DE
    region: EMEA
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: October 25, 2023
    date_ends: October 25, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: Washington, D.C.
    region: AMER
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: November 8, 2023
    date_ends: November 9, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: Tokyo, JP - Virutal
    region: APAC
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: D+CH GitLab Roadshow 2023
    type: MeetUp
    date_starts: April 18, 2023
    date_ends: November 14, 2023
    description: GitLab are on the road and coming to a location near you! Join D+CH GitLab Roadshow 2023 and learn more about DORA and SBOM. We'll bring some interesting content, networking opportunities and a tasty experience! Join the GitLab Roadshow 2023 in one of the main cities in Germany (and Zurich) and reserve your spot today!
    location: Various Germany cities
    region: EMEA
    social_tags:
    event_url: https://page.gitlab.com/dach-roadshows-events.html

  - topic: Crunch Data Conference 2023
    type: Conference
    date_starts: October 5, 2023
    date_ends: October 6, 2023
    description: A great chance to get the experience of DevSecOps and Data stories directly from producers of well-known and trusted DevSecOps platform with more than 30 million users. Firmly believe that story can aid and inspire an audience to try to do amazing things in their data world. You are in the right place if you want to find out how the GitLab Data team embraces DevOps and Open Source culture to move fast in a rapidly growing environment. We will put a spotlight on how to use the advantage of the internal product to make Data Engineers more agile and flexible in their daily job with a mission of assembling great data products.
    location: Budapest, Hungary
    region: EMEA
    social_tags:
    event_url: https://crunchconf.com/2023/talk/radovans-talk

  - topic: Inspiration 9
    type: Conference
    date_starts: September 29, 2023
    date_ends: September 29, 2023
    description: In the modern Data and Tech world, you should either live under a rock or have someone cut off electricity in your apartment to avoid any significant interaction with AI. Forget about hypes and buzzwords; let’s see AI in action making a developer’s life easier and super-efficient, with a small help of data, of course.
    location: Belgrade, Serbia
    region: EMEA
    social_tags:
    event_url: https://levi9conference.com/

  - topic: Data Science Conference Europe 2023
    type: Conference
    date_starts: November 20, 2023
    date_ends: November 24, 2023
    description: A great chance to hear the experience about the future of work from the one of the biggest all-remote company in the world. Find out more about the async work tips and tricks and leverage the context from the distributed work leaders. From the GitLab Data Team member, a first-face story about how the asynchronous way of working helps us manage the “chaos” (as folks usually think about remote work) in the 24/7 work environment. Vibrant session to expose the picture of the pioneers and advocates of all-remote philosophy.
    location: Belgrade, Serbia
    region: EMEA
    social_tags:
    event_url: https://datasciconference.com/all-speakers/

  - topic: PyCon Sweden 2023
    type: Conference
    date_starts: November 09, 2023
    date_ends: November 10, 2023
    description: In the comprehensive tutorial about dbt and the Data transformation, a GitLab Data veteran will show the power of Data Base Transformer, also known as DBT, a rising star in the Data World. You will get the basic knowledge about a use case where, how and why to use DBT. This is an ideal chance to catch the best mutual venture for SQL and Python.
    location: Stockholm, Sweden
    region: EMEA
    social_tags:
    event_url: https://www.pycon.se

  - topic: GitLab Hackathon
    type: Community Event
    date_starts: December 11, 2023
    date_ends: December 18, 2023
    description: The Hackathon is a virtual event open to anyone who is interested in contributing code, documentation, translations, UX designs and more to GitLab. Prizes are awarded to participants for having merge requests (MRs) merged.
    location: Virtual
    region: Global
    social_tags:
    event_url: https://about.gitlab.com/community/hackathon/
    featured:
    background: /images/community/community-banner.png

  - topic: Hack in the Box - Phuket
    type: Security Conference
    date_starts: August 21, 2023
    date_ends: August 25, 2023
    description: Automated Incident Response - GitLab SIRT goes to Phuket to teach attendees how to develop multiple tools to automate and standardize incident response processes.
    location: Phuket, Thailand
    region: Global
    event_url: https://conference.hitb.org/hitbsecconf2023hkt/

  - topic: GitLab Connect Sydney
    type: GitLab Connect
    date_starts: August 16, 2023
    date_ends: August 16, 2023
    description: GitLab Connect  is the can't miss event  that connects you to the ideas, technologies, and people that are driving business and software transformation. Get inspired by industry leaders, connect with peers, and gain game-changing DevSecOps best practices, as well as insights into the GitLab roadmap. You'll learn from our customers driving innovation as well as key partners as they share their approach on how they can help you on your DevSecOp journey.
    location: Sydney, Australia
    region: APAC
    social_tags:
    event_url: https://page.gitlab.com/event_August16_Connect_Sydney_APAC.html

  - topic: CISO Singapore 2023
    type: Conference
    date_starts: August 22, 2023
    date_ends: August 23, 2023
    description: Network with Singapore's Key InfoSec Decision Makers - expand your C-level network at our exclusive, must-attend VIP breakfast, lunch and dinner and find out how your peers are reporting cybersecurity risks to the board to achieve senior buy-in.
    location: Singapore
    region: APAC
    social_tags:
    event_url: https://ciso-sing.coriniumintelligence.com/

  - topic: Application Strategy Summit Melbourne
    type: Conference
    date_starts: August 22, 2023
    date_ends: August 22, 2023
    description: Applications are a fundamental compenent of how customers interact with business and have greatly empowered the productivity of the workforce. In just under a decade they have gone from a ‘nice to have’ to a must have for many organisations and a necessity for any modern business. This summit will be an opportunity to gain strategic and technical understanding from some of Australia's senior leaders in technology, applications, architecture and engineering.
    location: Melbourne, Australia
    region: APAC
    social_tags:
    event_url: https://forefrontevents.co/event/application-strategy-summit-vic-2023/

  - topic: AWS Public Sector Symposium Canberra
    type: Conference
    date_starts: August 30, 2023
    date_ends: August 30, 2023
    description: Join us for AWS Public Sector Symposium Canberra 2023, and discover how organisations of all sizes are using Amazon Web Services (AWS) to accelerate innovation and drive their missions forward. Explore how the cloud can help you enhance security, analyse data at scale, advance sustainability, and achieve your mission—faster and at lower cost. Returning in 2023 with all-new content and speakers, this must-attend event features inspiring keynotes from Australia’s tech leaders, breakout sessions, hands-on workshops, networking hubs, and more.
    location: Canberra, Australia
    region: APAC
    social_tags:
    event_url: https://aws.amazon.com/government-education/symposiums/canberra/

  - topic: Future of Security, Sydney 2023
    type: Conference
    date_starts: August 30, 2023
    date_ends: August 30, 2023
    description: The Future of Security, Sydney is FST Media's dedicated security forum for New South Wales, exploring the most pressing issues of cybersecurity, resilience and diligence in the financial services industry. Join us as we help you navigate the key post-pandemic trends in 2023, including digital transformation accelerating cyber-attacks, data security concerns with the next phase of Open Banking, and partnering with key stakeholders and financial regulators to shape security practices.
    location: Sydney, Australia
    region: APAC
    social_tags:
    event_url: https://fst.net.au/event/future-of-security-sydney-2023/
  
  - topic: GitLab India Meetup
    type: Meetup
    date_starts: August 31, 2023
    date_ends: August 31, 2023
    description: A virtual meetup hosted by the GitLab India meetup group. All are welcome to attend. 
    location: Virtual 
    region: APAC 
    social_tags: 
      - GitLabMeetup 
    event_url: https://www.meetup.com/gitlab-meetup-india/events/295171911/

  - topic: DevOps Summit Singapore
    type: Conference
    date_starts:  September 28, 2023
    date_ends: September 28, 2023
    description: The DevOps Summit will bring together 100+ senior ICT and business leaders to discuss the latest trends and best practices in DevOps. The Summit will offer the opportunity for leaders to network, brainstorm and decide future strategies for success. Whilst many are now well on their way with the journey, the question is, what does it take to develop a World Class DevOps strategy?
    location: Singapore
    region: APAC
    social_tags:
    event_url: https://forefrontevents.co/event/devops-summit-sg-2023/

  - topic: CIMICON Evolution
    type: Conference
    date_starts: October 4, 2023
    date_ends: October 6, 2023
    description: Europe's flagship event for corporate Intelligence and Insights professionals. CIMICON Evolution returns to Berlin for its 12th edition in 2023! As the world's foremost practitioner-driven knowledge exchange platform, we bring together key stakeholders shaping the corporate intelligence and insights scene. Join us and engage in meaningful discussions with your peers, explore challenges and opportunities, and gain insights into successful technology implementations that drive real results in enterprises. It's an excellent opportunity to expand your network and stay at the forefront of intelligence trends.
    location: Berlin, Germany
    region: EMEA
    social_tags:
    event_url: https://www.competitive-market-intelligence.com/

  - topic: DrupalCon Lille 2023
    type: Conference
    date_starts:  October 18, 2023
    date_ends: October 20, 2023
    description: DrupalCon comes back to France in 2023 between 17-20 October! This time the Lille Grand Palais is our host venue in Lille. Easily accessible with only a 35 minute train ride from Brussels and 80 minutes from London by train.
    location: Lille
    region: EMEA
    social_tags:
    event_url: https://events.drupal.org/lille2023/session/next-drupal-admin-ui-improvements
    
  - topic: AWS Public Sector Symposium New Delhi
    type: Conference
    date_starts: September 22, 2023
    date_ends: September 22, 2023
    description: Empowering organizations to build the India of tomorrow - join us at AWS Public Sector Symposium New Delhi 2023! The one-day agenda features inspiring keynotes, breakout sessions, hands-on workshops, networking hubs, and more. 18+ breakout sessions will cover topics ranging from artificial intelligence and big data analytics, to service transformation and skills empowerment. 
    location: New Delhi, India
    region: APAC
    social_tags:
    event_url: https://aws.amazon.com/government-education/symposiums/new-delhi/
    
  - topic: Public Sector Day Singapore 
    type: Conference
    date_starts: October 5th, 2023
    date_ends: October 5th, 2023
    description: Public Sector Day Singapore is an event curated for the public sector community including government, education, healthcare, and non-profit organizations. Explore how you can power data-driven experience, achieve sustainable outcomes, and enhance security — faster and at lower cost. Public Sector Day, hosted by GovInsider and AWS, offers valuable insights for organizations that face challenges achieving complex missions with limited resources. This event will bring together government, education, healthcare, government-linked companies (GLCs), and non-profit organizations to connect and reimagine public service delivery. 
    location: Singapore
    region: APAC
    social_tags:
    event_url: https://govinsider.asia/intl-en/event/public-sector-day-singapore 
    
  - topic: Modern DevOps Melbourne
    type: Conference 
    date_starts: October 9th, 2023
    date_ends: October 9th, 2023
    description: Modern DevOps Melbourne is your one stop shop for the latest best practices and solutions to maximise deployment scale and velocity. Covering modern approaches from platform engineering, DevSecOps, AI and MLOps, be part of the event that will shake up your DevOps delivery and help you get a competitive advantage.
    location: Melbourne, Australia
    region: APAC 
    social_tags:
    event_url: https://devops-mel.coriniumintelligence.com/
  
  - topic: Cloud Expo Asia 2023 
    type: Conference
    date_starts: October 11, 2023
    date_ends: October 12, 2023
    description: Cloud Expo Asia, Asia's best-attended technology event, returns on 11-12 October 2023 for its 9th edition at Marina Bay Sands, Singapore. The award-winning event connects technologists and business leaders with experts, solutions and services to help accelerate digital transformation plans. Come find us at our booth, attend our workshop or join us at our speaking session! 
    location: Singapore
    region: APAC
    social_tags:
    event_url: https://www.cloudexpoasia.com/
  
  - topic: GovWare Singapore 2023
    type: Conference 
    date_starts: October 17, 2023
    date_ends: October 19, 2023 
    description: Join us at the highly anticipated return of the GovWare Conference and Exhibition on 17–19 October at Sands Expo and Convention Centre, Singapore! This gathering of great minds will once again see over 10,000 global cybersecurity practitioners, policy-makers and organisational leaders converge to spark essential conversations and collaborations that will shape our digital future. Under the theme "Fostering Trust Through Collaboration in the New Digital Reality", GovWare 2023, a part of the renowned Singapore International Cyber Week (SICW), is set to deliver transformative insights that address the ever-evolving cybersecurity landscape.
    location: Singapore
    region: APAC
    social_tags:
    event_url: https://www.govware.sg/govware/2023/event-info
    
  - topic: DevOps Summit NSW 2023
    type: Conference
    date_starts: October 26, 2023
    date_ends: October 26, 2023
    description: DevOps has become an integral component of most businesses' technology strategy. For organisations to satisfy the needs of users and customers whilst maintaining a competitive edge, having the right tools, processes, and culture in software development and delivery is crucial. Whilst many are now well on their way with the journey, the question is, what does it take to develop a World Class DevOps strategy? The DevOps Summit will bring together 100+ senior ICT and business leaders to discuss the latest trends and best practices in DevOps. The Summit will offer the opportunity for leaders to network, brainstorm and decide future strategies for success. 
    location: Sydney, Australia
    region: APAC
    social_tags:
    event_url: https://forefrontevents.co/event/devops-summit-nsw-2023/
