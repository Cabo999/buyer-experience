---
  title: ソフトウェアサプライチェーンセキュリティ
  description: ソフトウェアサプライチェーンを保護し、脅威ベクトルの一歩先を行き、コンプライアンスへの準拠に役立つポリシーを定めて、より迅速に安全なソフトウェアを提供できるようにしましょう。
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note:
          -  組み込まれた自動化とポリシーの実施
        title: ソフトウェアサプライチェーンセキュリティ
        subtitle: ソフトウェアサプライチェーンを保護し、脅威ベクトルの一歩先を行き、コンプライアンスへの準拠に役立つポリシーを定めて、より迅速に安全なソフトウェアを提供できるようにしましょう。
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Ultimateを無料で試用
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: 価格を確認
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/resources/resources_11.jpeg
          image_url_mobile: /nuxt-images/supply-chain-hero.png
          alt: "画像:ソフトウェアサプライチェーンセキュリティ"
          bordered: true
    - name: 'by-industry-intro'
      data:
        intro_text: "お客様:"
        logos:
          - name: Bendigo and Adelaide Bank Logo
            image: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
            url: /customers/bab/
            aria_label: Bendigo and Adelaide Bankの導入事例へのリンク
          - name: Hackerone Logo
            image: /nuxt-images/logos/hackerone-logo.png
            url: /customers/hackerone/
            aria_label: HackerOneの事例へのリンク
          - name: new10 Logo
            image: /nuxt-images/logos/new10.svg
            url: /customers/new10/
            aria_label: The Zebraの導入事例へのリンク
          - name: The Zebra Logo
            image: /nuxt-images/logos/zebra.svg
            url: /customers/thezebra/
            aria_label: The Zebraの導入事例へのリンク
          - name: Chorus Logo
            image: /nuxt-images/logos/chorus.svg
            url: /customers/chorus/
            aria_label: Chorusの導入事例へのリンク
          - name: Hilti Logo
            image: /nuxt-images/logos/hilti_logo.svg
            url: /customers/hilti/
            aria_label: ヒルティの導入事例へのリンク
    - name: 'side-navigation-variant'
      links:
        - title: 概要
          href: '#overview'
        - title: 機能
          href: '#capabilities'
        - title: 顧客
          href: '#customers'
        - title: 価格
          href: '#pricing'
        - title: リソース
          href: '#resources'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: エンドツーエンドでのソフトウェアサプライチェーンの保護
                is_accordion: false
                items:
                  - icon:
                      name: continuous-integration
                      alt: 継続的インテグレーションアイコン
                      variant: marketing
                    header: ソフトウェア開発ライフサイクルの保護
                    text: コードやビルド、依存関係、リリースアーティファクトなど、複数の攻撃対象領域を保護します
                    link_text: DevSecOpsの詳細について
                    link_url: /solutions/security-compliance/
                    ga_name: reduce security learn more
                    ga_location: benefits
                  - icon:
                      name: devsecops
                      alt: DevSecOpsアイコン
                      variant: marketing
                    header: コンプライアンス要求事項を遵守する
                    text: 監査レポートとガバナンスレポートに簡単にアクセスできます
                    link_text: GitLabを選ぶ理由
                    link_url: /why-gitlab/
                    ga_name: free up learn more
                    ga_location: benefits
                  - icon:
                      name: release
                      alt: チェックマーク付きの盾アイコン
                      variant: marketing
                    header: ガードレールの実装
                    text: アクセスを制御し、ポリシーを実装します
                    link_text: プラットフォームのアプローチの詳細について
                    link_url: /solutions/devops-platform/
            - name: 'solutions-video-feature'
              data:
                video:
                  url: 'https://player.vimeo.com/video/762685637?h=f96e969756'
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              data:
                subtitle: コード、ビルド、リリースを安全に。
                sub_description: ''
                white_bg: true
                markdown: true
                sub_image: /nuxt-images/solutions/supply-chain.png
                solutions:
                  - title: ゼロトラストの確立
                    description: |
                      IDおよびアクセス管理(IAM)は、ソフトウェアサプライチェーンにおける最大の攻撃ベクトルの1つです。GitLabでは、あなたの環境で動作するすべての人物の識別子とマシンの識別子を認証、承認、そして継続的に検証を行うことによってアクセスを保護します。
                      *  [2要素認証](https://docs.gitlab.com/ee/security/two_factor_authentication.html)などのきめ細かな[アクセス制御](https://docs.gitlab.com/ee/user/admin_area/settings/visibility_and_access_controls.html)を実装する
                      *  [トークンの有効期限ポリシー](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)を定める
                      *  組織のルールや規制上のルールに基づいて、[ポリシー](https://docs.gitlab.com/ee/administration/compliance.html#policy-management)を設定する
                      *  コンプライアンスへの準拠のために、包括的な[監査レポートとガバナンスレポート](https://docs.gitlab.com/ee/administration/audit_reports.html)を生成する
                      *  追加のガードレールとして、[2名体制での承認](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html)を実施する
                  - title: ソースコードの保護
                    description: |
                      コードにアクセスできるユーザー、およびコードへの変更のレビュー方法とマージ方法を管理することによって、ソースコードのセキュリティと整合性を確保します。

                      *  ソースコードのバージョン管理、[コード履歴](https://docs.gitlab.com/ee/user/project/repository/git_history.html?_gl=1*1ngzpgw*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2NDUzNDg3My4xMjkuMS4xNjY0NTM4MjQ3LjAuMC4w)、[アクセス制御](https://docs.gitlab.com/ee/user/admin_area/settings/visibility_and_access_controls.html)を確立する
                      *  自動化された[コード品質](https://docs.gitlab.com/ee/ci/testing/code_quality.html)テストを実施して、変更によるパフォーマンスへの影響を分析する
                      *  レビューと[承認ルール](https://docs.gitlab.com/ee/ci/testing/code_quality.html)を実施し、本番環境に反映される内容を制御する
                      *  [自動セキュリティスキャン](https://docs.gitlab.com/ee/user/application_security/)を実行し、コードがマージされる前に脆弱性を検出する
                      *  自動化された[秘密検出](https://docs.gitlab.com/ee/user/application_security/secret_detection/)によって、パスワードや機密情報がソースコードに含まれていないことを確認する
                      *  [署名済みコミット](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/)を導入して、デベロッパーのなりすましを防ぐ
                  - title: 依存関係の保護
                    description: |
                      プロジェクトで使用されているすべてのオープンソースの依存関係に公開されている脆弱性が含まれていないこと、また信頼できるソースから入手したものであり、改ざんされていないことを確認します。

                      *  自動的に[ソフトウェア部品表](https://docs.gitlab.com/ee/user/application_security/dependency_list/)を生成して、プロジェクトの依存関係を特定する
                      *  自動化された[ソフトウェア構成分析](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)によって、使用されており依存関係にあるソフトウェアの脆弱性を特定する
                      *  [ライセンスコンプライアンス](https://docs.gitlab.com/ee/user/compliance/license_compliance/)スキャンを実行して、プロジェクトにおいて組織のポリシーに沿ってソフトウェアのライセンスが使用されていることを確認する
                  - title: ビルド環境の保護
                    description: |
                      悪意のある人物が不正コードをビルドプロセスに追加することや、パイプラインによってビルドされたソフトウェアの制御を得たり、パイプラインで使用されている秘密にアクセスしたりすることを防止します。

                      *  [ビルド環境を隔離して](https://docs.gitlab.com/runner/security/?_gl=1*1d95r9z*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2NDUzNDg3My4xMjkuMS4xNjY0NTM4MDA2LjAuMC4w)、不正アクセスや不正コードの実行を防ぐ
                      *  リリースに含まれるすべてのものの[リリースエビデンス](https://docs.gitlab.com/ee/user/project/releases/#release-evidence)を保持する
                      *  [ビルドアーティファクトの認証](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#artifact-attestation)によって、ビルドアーティファクトが漏えいしていないことを保証する
                  - title: リリースアーティファクトの保護
                    description: |
                      攻撃者がアプリケーションのデザインや設定の弱点を悪用して個人データを盗んだり、アカウントに不正にアクセスしたり、正規ユーザーになりすましたりするのを阻止します。

                      *  クラスターへの[安全な接続](https://about.gitlab.com/blog/2022/01/07/gitops-with-gitlab-using-ci-cd/#meet-the-cicd-tunnel)を確立して、リリースアーティファクトを提供する
                      *  デプロイ前に、[実行中のアプリケーションでセキュリティ上の脆弱性](https://docs.gitlab.com/ee/user/application_security/dast/)を特定する
                      *  実行中のアプリケーションが[APIインターフェース](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/)を通じてさらされることのないようにする
        - name: 'div'
          id: 'customers'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              data:
                align: left
                header: |
                  企業に信頼され、
                  <br />
                  デベロッパーに愛用されています。
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
                      alt: Bending and Adelaide Bankロゴ
                    quote: 私たちには今、デジタルトランスフォーメーションの目標に沿った、常に革新し続けるソリューションがあります。
                    author: Bendigo and Adelaide Bank、DevOps Enablement主任、Caio Trevisan氏
                    ga_carousel: bending and adelaide bank testimonial
                    url: /customers/bab/
                  - title_img:
                      url: /nuxt-images/logos/new10.svg
                      alt: New10ロゴ
                    quote: GitLabはKubernetesやServerlessに対応し、DASTやSASTのような素晴らしいセキュリティ機能をサポートしているため、当社のとてもモダンなアーキテクチャで非常に役立っています。GitLabによって、最先端を行くアーキテクチャを実現できています。
                    author: 最高技術責任者、Kirill Kolyaskin氏
                    ga_carousel: New10 testimonial
                    url: /customers/new10/
                  - title_img:
                      url: /nuxt-images/logos/zebra.svg
                      alt: The Zebraロゴ
                    quote: (GitLabの)最大の価値は、開発チームがデプロイプロセスでより大きな役割を果たせるようになることです。以前は、どのような仕組みであるかを本当にわかっている人はほんのわずかでしたが、今では開発組織全体がCIパイプラインの仕組みを知っていて、使用することができ、新しいサービスを追加し、インフラストラクチャがボトルネックになることなく本番環境に移行できます。
                    author: The Zebra、シニアソフトウェアマネージャー、Dan Bereczki氏
                    url: /customers/thezebra/
                    ga_carousel: the zebra testimonial
                  - title_img:
                      url: /nuxt-images/logos/hilti_logo.svg
                      alt: ヒルティロゴ
                    quote: GitLabはスイートのようにバンドルされており、非常に洗練されたインストーラーが付属しています。そしてどういうわけかうまく機能します。これは、ただ立ち上げて稼動させたい企業にとっては非常に便利です。
                    author: ヒルティ、ソフトウェアデリバリ主任、Daniel Widerin氏
                    url: /customers/hilti/
                    ga_carousel: hilti testimonial
        - name: div
          id: 'pricing'
          slot_enabled: true
          slot_content:
            - name: 'tier-block'
              class: 'slp-mt-96'
              id: 'pricing'
              data:
                header: あなたに最適なプランはどれですか？
                cta:
                  url: /pricing/
                  text: 価格の詳細について
                  data_ga_name: pricing
                  data_ga_location: free tier
                  aria_label: 価格
                tiers:
                  - id: free
                    title: Free
                    items:
                      - 静的アプリケーションセキュリティテスト(SAST)と秘密検出
                      - jsonファイルの所見
                    link:
                      href: /pricing/
                      text: 利用を開始する
                      data_ga_name: pricing
                      data_ga_location: free tier
                      aria_label: Freeプラン
                  - id: premium
                    title: Premium
                    items:
                      - 静的アプリケーションセキュリティテスト(SAST)と秘密検出
                      - jsonファイルの所見
                      - MRの承認とその他の一般的な制御
                    link:
                      href: /pricing/
                      text: 詳しく見る
                      data_ga_name: pricing
                      data_ga_location: premium tier
                      aria_label: Premiumプラン
                  - id: ultimate
                    title: Ultimate
                    items:
                      - Premiumの機能に加え、以下の機能が含まれます
                      - 包括的なセキュリティスキャナー(SAST、DAST、秘密、依存関係、コンテナ、IaC、API、クラスターイメージ、ファジングテストなど)
                      - MRパイプライン内での実行可能な結果
                      - コンプライアンスパイプライン
                      - セキュリティとコンプライアンスのダッシュボード
                      - その他たくさん
                    link:
                      href: /pricing/
                      text: 詳しく見る
                      data_ga_name: pricing
                      data_ga_location: ultimate tier
                      aria_label: Ultimateプラン
                    cta:
                      href: /free-trial/
                      text: Ultimateを無料で試す
                      data_ga_name: pricing
                      data_ga_location: ultimate tier
                      aria_label: Ultimateプラン
        - name: 'div'
          id: 'resources'
          slot_enabled: true
          slot_content:
          - name: solutions-resource-cards
            data:
              title: 関連するリソース
              align: left
              column_size: 4
              grouped: true
              cards:
                - icon:
                    name: video
                    variant: marketing
                    alt: ビデオアイコン
                  event_type: ビデオ
                  header: セキュリティのシフトレフト - DevSecOpsの概要
                  link_text: "今すぐ視聴"
                  image: "/nuxt-images/solutions/dev-sec-ops/shifting-security-left.jpeg"
                  href: "https://www.youtube.com/embed/XnYstHObqlA"
                  data_ga_name: Shifting Security Left - DevSecOps Overview
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: ビデオアイコン
                  event_type: ビデオ
                  header: GitLabを用いた脆弱性の管理と職務分掌の実施
                  link_text: "今すぐ視聴"
                  image: /nuxt-images/solutions/dev-sec-ops/managing-vulnerabilities.jpeg
                  href: https://www.youtube.com/embed/J5Frv7FZtnI
                  data_ga_name: Managing Vulnerabilities and Enabling Separation of Duties with GitLab
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: ビデオアイコン
                  event_type: ビデオ
                  header: GitLab 15のリリース - 新しいセキュリティ機能
                  link_text: "今すぐ視聴"
                  image: "/nuxt-images/solutions/dev-sec-ops/gitlab-15-release.jpeg"
                  href: https://www.youtube.com/embed/BasGVNvOFGo
                  data_ga_name: GitLab 15 Release - New Security Features
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: ビデオアイコン
                  event_type: ビデオ
                  header: SBOMと認証
                  link_text: "今すぐ視聴"
                  image: "/nuxt-images/solutions/compliance/sbom-and-attestation.jpeg"
                  href: https://www.youtube.com/embed/wX6aTZfpJv0
                  data_ga_name: SBOM and Attestation
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: 電子書籍アイコン
                  event_type: "電子書籍"
                  header: "ソフトウェアサプライチェーンのセキュリティガイド"
                  link_text: "詳しく見る"
                  image: "/nuxt-images/resources/resources_13.jpeg"
                  href: https://cdn.pathfactory.com/assets/10519/contents/360915/35d042c6-3449-4d50-b2e9-b08d9a68f7a1.pdf
                  data_ga_name: "Guide to software supply chain security"
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: 電子書籍アイコン
                  event_type: "電子書籍"
                  header: "GitLab DevSecOpsアンケート"
                  link_text: "詳しく見る"
                  image: "/nuxt-images/resources/resources_1.jpeg"
                  href: https://cdn.pathfactory.com/assets/10519/contents/432983/c6140cad-446b-4a6c-96b6-8524fac60f7d.pdf
                  data_ga_name: " GitLab DevSecOps survey"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: ブログアイコン
                  event_type: ブログ
                  header: "ソフトウェアサプライチェーンのセキュリティの究極ガイド"
                  link_text: "詳しく見る"
                  href: https://about.gitlab.com/blog/2022/08/30/the-ultimate-guide-to-software-supply-chain-security/
                  image: /nuxt-images/blogimages/hotjar.jpg
                  data_ga_name: "Ultimate guide to software supply chain security "
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: ブログアイコン
                  event_type: ブログ
                  header: "GitLabで米国国立標準技術研究所(NIST)のフレームワークに準拠"
                  link_text: "詳しく見る"
                  href: https://about.gitlab.com/blog/2022/03/29/comply-with-nist-secure-supply-chain-framework-with-gitlab/
                  image: "/nuxt-images/resources/resources_7.jpeg"
                  data_ga_name: "Comply to NIST framework with GitLab "
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: ブログアイコン
                  event_type: ブログ
                  header:  "ソフトウェアサプライチェーンを保護するための優れたチーム戦略"
                  link_text: "詳しく見る"
                  href: https://about.gitlab.com/blog/2022/01/06/elite-team-strategies-to-secure-software-supply-chains/
                  image: "/nuxt-images/resources/resources_13.jpeg"
                  data_ga_name: "Elite team strategies to secure the software supply chain "
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: ブログアイコン
                  event_type: ブログ
                  header: "自動認証によるソフトウェアサプライチェーンの保護"
                  link_text: "詳しく見る"
                  href: https://about.gitlab.com/blog/2022/08/10/securing-the-software-supply-chain-through-automated-attestation/
                  image: "/nuxt-images/resources/resources_4.jpeg"
                  data_ga_name: "Securing the software supply chain through automated attestation"
                  data_ga_location: resource cards
                - icon:
                    name: report
                    alt: Report Icon
                    variant: marketing
                  event_type: "アナリストレポート"
                  header: "GitLabが2022 Gartner Magic Quadrantで挑戦者に認定"
                  link_text: "詳しく見る"
                  href: "https://about.gitlab.com/analysts/gartner-ast22/"
                  image: /nuxt-images/blogimages/hemmersbach_case_study.jpg
                  data_ga_name: "GitLab a challenger in 2022 Gartner Magic Quadrant"
                  data_ga_location: resource cards
    - name: solutions-cards
      data:
        title: GitLabでさらに多くのことを実現
        column_size: 4
        link :
          url: /solutions/
          text: その他のソリューションを確認
          data_ga_name: solutions explore more
          data_ga_location: body
        cards:
          - title: DevSecOps
            description: GitLabは、ソフトウェアデリバリを自動化し、エンドツーエンドのソフトウェアサプライチェーンを保護することで、スピードとセキュリティの両立を可能にします。
            icon:
              name: devsecops
              alt: devsecopsアイコン
              variant: marketing
            href: /solutions/security-compliance/
            cta: 詳しく見る
            data_ga_name: devsecops learn more
            data_ga_location: body
          - title: 継続的なソフトウェアコンプライアンス
            description: GitLabなら、DevSecOpsライフサイクルにセキュリティを統合するのも簡単です。
            icon:
              name: build
              alt: ビルドアイコン
              variant: marketing
            href: /solutions/continuous-software-compliance/
            cta: 詳しく見る
            data_ga_name: continuous software compliance learn more
            data_ga_location: body
          - title: 継続的インテグレーションとデリバリ
            description: ソフトウェアデリバリをオンデマンドで繰り返し実現します。
            icon:
              name: continuous-delivery
              alt: 継続的デリバリ
              variant: marketing
            href: /solutions/continuous-integration/
            cta: 詳しく見る
            data_ga_name: siemens learn more
            data_ga_location: body

