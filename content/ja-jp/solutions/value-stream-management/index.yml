---
  title: バリューストリーム管理
  description: GitLabのバリューストリーム管理は、価値の創出までの時間を短縮し、ビジネス成果を最適化して、ソフトウェアの品質を向上させるための体系的な方法を提供します。
  report_cta:
    layout: "dark"
    title: アナリストレポート
    reports:
    - description: "GitLabは、The Forrester Wave™: Integrated Software Delivery Platformsの2023年度第2四半期における唯一のリーダーに認定されました"
      link_text: レポートを読む
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
    - description: "2023年 Gartner® DevOpsプラットフォームのMagic Quadrant™でGitLabがリーダーの1社に位置付け"
      link_text: レポートを読む
      url: /gartner-magic-quadrant/
  components:
    - name: 'solutions-hero'
      data:
        title: バリューストリーム管理
        subtitle: DevSecOpsライフサイクルのビジネス価値を測定し、管理します。
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: 無料トライアルを開始する
          url: /free-trial/
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "画像: gitlab + バリューストリーム管理"
    - name: 'copy-media'
      data:
        block:
          - header: 'バリューストリーム管理のメリット'
            id: benefits-of-value-stream-management 
            miscellaneous: |
              ソフトウェア開発は、常に顧客やビジネスにおける価値の創出を最大限にすることを目指すべきですが、その創出における非効率性をどのように特定してコースを修正すればいいでしょうか？ GitLabのバリューストリーム管理は、企業がエンドツーエンドのDevSecOpsワークストリームを可視化し、無駄や非効率性を特定して最高の価値を創出するためにワークストリームを最適化するのに役立ちます。
          - header: 'バリューストリーム配信プラットフォーム'
            id: a-value-stream-delivery-platform 
            miscellaneous: |
              [The Future of DevOps Toolchains Will Involve Maximizing Flow in IT Value Streams](https://www.gartner.com/en/documents/3979558/the-future-of-devops-toolchains-will-involve-maximizing){data-ga-name="The Future of DevOps Toolchains Will Involve Maximizing Flow in IT Value Streams" data-ga-location="body"}で、Gartnerは「DevOpsツールチェーンの選択と展開を担当するインフラストラクチャおよび運用のリーダーは、複雑なツールチェーンの管理費を削減するDevOpsバリューストリーム配信プラットフォームを活用してビジネス能力を推進する必要がある」と勧告しています。 [1]単一アプリケーションとして[全体のDevOpsプラットフォーム](/solutions/devops-platform/){data-ga-name="GitLab - The DevOps Platform" data-ga-location="body"} を提供することにより、GitLabは「ツールチェーン税」なしでライフサイクル全体にわたってエンドツーエンドの可視性を提供するのに適しています。 作業が行われる場所として、GitLabは可視化とアクションを統合させることで、ユーザーがコンテキストを失わずにいつでも学習から実行に移ることができます。

              *[1] Gartner "The Future of DevOps Toolchains Will Involve Maximizing Flow in IT Value Streams," Manjunath Bhat, et al, 14 January 2020 (Gartnerサブスクリプションが必要)*
            media_link_href: https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html
            media_link_text: バリューストリーム分析
            media_link_data_ga_name: Value Stream Analytics
            media_link_data_ga_location: body
          - header: 'エンドツーエンドのプロセスの表示と管理'
            id: view-and-manage-end-to-end-processes
            text: |
              バリューストリーム分析では、アイデアから顧客デリバリーまでのDevSecOpsフローを可視化および管理するのに役立ちます。GitLabは、追加のインストールや設定なしで一般的なワークフローやメトリクスに基づいた実行可能なレポートを提供しています。より深く掘り下げたり、カスタムワークフローをモデル化したい場合は、GitLabの包括的な統合データストアを使って重要なイベントを簡単に追跡できます。

              * **1つのツール、チェーンなし:** GitLabは、DevSecOpsツールチェーン全体を単一アプリケーションに統合します。管理するインテグレーションがなく、可視性を制限するAPIチョークポイントもありません。また、役割に関係なく社内の全員が共有できる経験を得ることができます。

              * **共有された実用的なデータ:** 単一の作業システム上に構築された単一の洞察ソースは、価値創出に費やす時間を増やし、停滞している場所を見つける時間を減らすことができます。

              * **形式ではなく価値に焦点を当てる:** 実際の作業フローを追跡および管理し、不必要な形式を排除します。 単一のDevSecOpsアプリケーションを使用することで、実際の作業アイテムへワンクリックでドリルダウンでき、即座にブロックを削除することができます。

            video:
              video_url: https://www.youtube.com/embed/8pLEucNUlWI
            inverted: true
          - header: '措置'
            id: measure
            text: |
              バリューストリームからのデータに基づいて継続的な改善を推進します。

              * **追跡フローと加速:** 顧客への価値フローに焦点を当てることは、デリバリープロセスを効率化する鍵です。
              ***サイクルタイム:** 実際の作業と労力、価値を提供するための実際のサイクルタイムを追跡します。
              * **ビジネス価値:** 実際の価値を変更にリンクさせます。
              * **DORA 4メトリクス:** [DORA 4メトリクス](https://about.gitlab.com/solutions/value-stream-management/dora/)はDevSecOpsの成熟度をベンチマークとして評価する指標で、チーム内や他の企業・業界との比較に役立ちます。DevSecOpsプロセスの効率性を測定するためには、[変更のリードタイム](https://docs.gitlab.com/ee/api/dora4_project_analytics.html#list-project-merge-request-lead-times){data-ga-name="DORA4 Analytics Project API | GitLab" data-ga-location="body"} と[デプロイ頻度](https://docs.gitlab.com/ee/api/dora4_project_analytics.html){data-ga-name="DORA4 Analytics Project API | GitLab" data-ga-location="body"}をモニタリングすることが重要です。
            image:
              image_url: /nuxt-images/solutions/tasks-by-type.png
              alt: ""
              caption: |
                [戦略により合った実践を可能にするために、異なるタイプの作業を通したビジネス価値の配分を把握することが重要です。](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html#type-of-work---tasks-by-type-chart){data-ga-name="Value Stream Analytics | GitLab" data-ga-location="body"}
          - header: '管理'
            id: manage
            text: |
              バリューストリームは、アイデアの立ち上げから顧客までの新しいイノベーションフローを可視化し、管理するのに役立ちます。

              ***制約の特定**: バリューストリーム内の摩擦の原因を見つけ、リードタイムを最適化します。
              ***ボトルネックの除去:** 制約に対処するためのアクションを実行します。
              ***フローの最適化:** 組織全体でDevSecOpsのベストプラクティスを加速させます。
              ***効率の管理:** [詳細な洞察](https://docs.gitlab.com/ee/user/project/insights/){data-ga-name="Value Stream Analytics | GitLab" data-ga-location="body"}により、トリアージの処理、生成された問題やバグ、マージにかかる時間などを示すことで、開発プロセスの非効率性を簡単に特定し、修正することができます。
            image:
              image_url: /nuxt-images/solutions/days-to-complete.png
              alt: ""
              caption: |
                [プロセスを検査して改善し、傾向を把握して、外れ値の原因を特定します。](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html#days-to-completion-chart){data-ga-name="Insights | GitLab" data-ga-location="body"}
              vertical_centered: true
            inverted: true
    - name: 'solutions-carousel-videos'
      data:
        title: バリューストリーム管理
        videos:
          - title: "VSMエグゼクティブインサイト"
            photourl: /nuxt-images/video-carousels/vsm_1/1.jpg
            video_link: https://www.youtube-nocookie.com/embed/0MR7Q8Wiq6g
            carousel_identifier:
              - ''

          - title: "VSMデベロッパーインサイト"
            photourl: /nuxt-images/video-carousels/vsm_1/4.jpg
            video_link: https://www.youtube-nocookie.com/embed/JPYWrRByAYk
            carousel_identifier:
              - ''

          - title: "VSMセキュリティインサイト"
            photourl: /nuxt-images/video-carousels/vsm_1/5.jpg
            video_link: https://www.youtube-nocookie.com/embed/KSJ9VhKQAS0
            carousel_identifier:
              - ''

          - title: "VSMオペレーションインサイト"
            photourl: /nuxt-images/video-carousels/vsm_1/4.jpg
            video_link: https://www.youtube-nocookie.com/embed/lDWxH2YO3Yk
            carousel_identifier:
              - ''

          - title: "VSM生産性インサイト"
            photourl: /nuxt-images/video-carousels/vsm_1/5.jpg
            video_link: https://www.youtube-nocookie.com/embed/vYKC7r8ztVA
            carousel_identifier:
              - ''

          - title: "マッピングのためのVSM課題ボード"
            photourl: /nuxt-images/video-carousels/vsm_1/6.jpg
            video_link: https://www.youtube-nocookie.com/embed/9ASHiQ2juYY
            carousel_identifier:
              - ''

          - title: "VSMビジネスバリューモニタリング"
            photourl: /nuxt-images/video-carousels/vsm_1/7.jpg
            video_link: https://www.youtube-nocookie.com/embed/oG0VESUOFAI
            carousel_identifier:
              - ''
