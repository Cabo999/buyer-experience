---
  title: Verify
  description: Learn how GitLab helps keep strict quality standards for production code with automatic testing and reporting. View more here!
  components:
    - name: sdl-cta
      data:
        title: Verify
        aos_animation: fade-down
        aos_duration: 500
        subtitle:
          Keep strict quality standards for production code with automatic testing and reporting.
        text: |
          GitLab helps delivery teams fully embrace continuous integration to automate the builds, integration and verification of their code. GitLab’s industry leading CI capabilities enables automated testing, Static Application Security Testing, Dynamic Application Security Testing, and code quality analysis to provide fast feedback to developers and testers about the quality of their code. With pipelines that enable concurrent testing and parallel execution, teams quickly get insight about every commit, allowing them to deliver higher quality code faster.
        icon:
          name: verify-alt-2
          alt: Verify Spotlight Icon
          variant: marketing
    - name: featured-media
      data:
        column_size: 6
        header: Product categories
        header_animation: fade-up
        header_animation_duration: 500
        media:
          - title: Continuous Integration (CI)
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Gain the confidence to ship at blistering speed and immense scale with automated builds, testing, and out-of-the-box security to verify each commit moves you forward.
            link:
              href: /solutions/continuous-integration/
              text: Learn More
              data_ga_name: continuous integration learn more
              data_ga_location: body
          - title: Code Testing and Coverage
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Code testing and coverage ensure that individual components built within a pipeline perform as expected, and are an important part of a Continuous Integration framework.
            link:
              href: https://docs.gitlab.com/ee/ci/unit_test_reports.html
              text: Learn More
              data_ga_name: code testing and coverage learn more
              data_ga_location: body
          - title: Performance Testing
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Be confident in the performance of your changes by ensuring that they are validated against real world scenarios.
            link:
              href: https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html
              text: Learn More
              data_ga_name: performance testing learn more
              data_ga_location: body
          - title: Merge Trains
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Keeping master green and ensuring the stability of collaboration on branches is vitally important. GitLab has introduced Merge Trains as an important way to accomplish this.
            link:
              href: https://docs.gitlab.com/ee/ci/merge_request_pipelines/pipelines_for_merged_results/merge_trains/
              text: Learn More
              data_ga_name: merge trains learn more
              data_ga_location: body
          - title: Review Apps
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Get a fully functional pre-production environment for every merge request that updates on each commit. See code running, and enable user acceptance testing and automated smoke tests before you merge.
            link:
              href: /stages-devops-lifecycle/review-apps/
              text: Learn More
              data_ga_name: review apps learn more
              data_ga_location: body
          - title: Secrets Management
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
              Manage secrets and protect sensitive data to enable GitLab, or a component built within GitLab to connect to other systems.
            link:
              href: /direction/verify/secrets_management/
              text: Learn More
              data_ga_name: secrets management learn more
              data_ga_location: body
    - name: copy
      data:
        aos_animation: fade-up
        aos_duration: 500
        block:
          - align_center: true
            text: |
              Learn more about our roadmap for upcoming features on our [Direction page](/direction/verify/){data-ga-name="verify direction" data-ga-location="body"}.
    - name: sdl-related-card
      data:
        column_size: 4
        header: Related
        cards:
          - title: Create
            icon:
              name: create-alt-2
              alt: Create Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 500
            text: Create, view, manage code and project data through powerful branching tools.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/create/
              data_ga_name: create
              data_ga_location: body
          - title: Release
            icon:
              name: release-alt-2
              alt: Release Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 1000
            text: |
              GitLab's integrated CD solution allows you to ship code with zero-touch, be it on one or one thousand servers.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/release/
              data_ga_name: release
              data_ga_location: body
          - title: Configure
            icon:
              name: configure-alt-2
              alt: Configure Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 1500
            text: |
              Configure your applications and infrastructure.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/configure/
              data_ga_name: configure
              data_ga_location: body
