---
  title: Démarrer pour les petites entreprises
  description: 'Vous devez arriver rapidement sur le marché et innover plus vite que la concurrence. Ce guide vous montrera comment démarrer avec GitLab, la solution idéale pour les petites entreprises.'
  side_menu:
    anchors:
      text: Sur cette page
      data:
      - text: Pour commencer
        href: "#demarrer"
        data_ga_name: getting-started
        data_ga_location: side-navigation
      - text: Mise en place
        href: "#get-setup"
        data_ga_name: getting-setup
        data_ga_location: side-navigation
      - text: Utiliser GitLab
        href: "#utiliser-gitlab"
        data_ga_name: using-gitlab
        data_ga_location: side-navigation
  hero:
    crumbs:
      - title: Commencer
        href: /get-started/
        data_ga_name: Get Started
        data_ga_location: breadcrumb
      - title: Démarrer pour les petites entreprises
    header_label: 20 min pour compléter
    title: Démarrer pour les petites entreprises
    content: |
      "Vous devez arriver rapidement sur le marché et innover plus vite que la concurrence. Vous ne pouvez pas vous permettre d'être ralenti par un processus DevSecOps compliqué. Ce guide vous aidera à mettre en place rapidement les éléments essentiels au développement et à la livraison automatisés de logiciels sur le niveau Premium, avec des options permettant d'inclure la sécurité, la conformité et la planification de projet que l'on trouve dans le niveau Ultimate."
  copy_info:
    title: Avant de commencer
    text: |
      Dans GitLab 15.1 (22 juin 2022) et les versions ultérieures, les espaces de noms dans GitLab.com sur le niveau gratuit seront limités à cinq (5) membres par <a href="https://docs.gitlab.com/ee/user/namespace/" data-ga-name="namespaces" data-ga-location="body">espace de noms</a>. Cette limite s'applique aux groupes de premier niveau et aux espaces de noms personnels. Si vous avez plus d'utilisateurs, nous vous recommandons de commencer par un niveau payant.
  steps:
    groups:
      - header: Pour commencer
        show: Afficher tout
        hide: Cacher tout
        id: demarrer
        items:
          - title: Déterminer l'abonnement qui vous convient
            copies:
              - title: GitLab SaaS ou GitLab autogéré
                text: |
                  <p>Voulez-vous que GitLab gère votre plateforme GitLab ou souhaitez-vous la gérer vous-même ?</p>
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/#choose-between-gitlab-saas-or-gitlab-self-managed
                  text: Évaluer les différences
                  ga_name: GitLab Saas vs Self-Managed
                  ga_location: body
          - title: Déterminer le niveau qui répondra à vos besoins
            copies:
              - title: Gratuit, Premium ou Ultimate
                text: |
                  <p>Pour déterminer le niveau qui vous convient, tenez compte des éléments suivants : </p>

              - title: Nombre d'utilisateurs
                text: |
                  Les abonnements à GitLab utilisent un modèle concurrent (siège) pour le SaaS et l'autogestion. Le nombre d'utilisateurs/de sièges peut influencer le choix du niveau. Si vous avez plus de cinq utilisateurs, un niveau payant (Premium ou Ultimate) est nécessaire.
              - title: Quantité de stockage nécessaire
                text: |
                  Les espaces de noms gratuits sur GitLab SaaS ont une limite de stockage de 5GB.
              - title: Sécurité et conformité souhaitées
                text: |
                  <p>* La détection des secrets, le SAST et l'analyse des conteneurs sont disponibles dans les versions Free et Premium.</p>
                  <p>* Des scanners supplémentaires <a href="https://docs.gitlab.com/ee/user/application_security">tels que DAST</a>, dépendances, images Cluster, IaC, APIs, et fuzzing sont disponibles dans Ultimate.</p>
                  <p>* Les résultats exploitables, intégrés dans le pipeline des demandes de fusion et dans le tableau de bord de la sécurité, nécessitent une gestion optimale des vulnérabilités.</p>
                  <p>* Les pipelines de conformité requièrent de l'Ultimate.</p>
                  <p>* Découvrez nos <a href="https://docs.gitlab.com/ee/user/application_security/">scanners de sécurité</a> et nos <a href="https://docs.gitlab.com/ee/administration/compliance.html">capacités de mise en conformité</a>.
                link:
                  href: /pricing/
                  text: Consultez nos tarifs pour en savoir plus
                  ga_name: pricing
                  ga_location: body
      - header: Mise en place
        show: Afficher tout
        hide: Cacher tout
        id: get-setup
        items:
          - title: Créez votre compte d'abonnement SaaS
            copies:
              - title: Déterminez le nombre de sièges que vous souhaitez
                text: |
                  Un abonnement GitLab SaaS utilise un modèle concurrent (siège). Vous payez un abonnement en fonction du nombre maximum d'utilisateurs pendant la période de facturation. Vous pouvez ajouter et supprimer des utilisateurs pendant la période d'abonnement, tant que le nombre total d'utilisateurs à un moment donné ne dépasse pas le nombre d'abonnements.
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#how-seat-usage-is-determined
                  text: Apprenez comment l'utilisation des sièges est déterminée
                  ga_name: Determine how many seats you want
                  ga_location: body
              - title: Obtenir votre abonnement SaaS
                text: |
                  <p>GitLab SaaS est l'offre de logiciel-service de GitLab, disponible sur GitLab.com. Il n'est pas nécessaire d'installer quoi que ce soit pour utiliser GitLab SaaS, il suffit de s'inscrire. L'abonnement détermine les fonctionnalités disponibles pour vos projets privés. Allez sur la page de tarification et sélectionnez **Acheter Premium** ou **Acheter Ultimate**.<p/>
                  <p>Organisationen mit öffentlichen Open-Source-Projekten können sich aktiv für unser GitLab for Open Source-Programm bewerben. Funktionen von <a href="/pricing/ultimate">GitLab Ultimate</a>, einschließlich 50.000 Minuten berechnen, stehen qualifizierten Open-Source-Projekten über das <a href="/solutions/open-source">GitLab for Open Source</a> Programm kostenlos zur Verfügung.<p/>
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#obtain-a-gitlab-saas-subscription
                  text: En savoir plus sur l'abonnement SaaS
                  ga_name: Obtain your SaaS subscription
                  ga_location: body
              - title: Déterminer les minutes d'exécution partagée CI/CD nécessaires
                text: |
                  <a href="https://docs.gitlab.com/ee/ci/runners/runners_scope.html#shared-runners" data-ga-name="Shared Runners" data-ga-location="body">Gestionnaires partagés</a> sont partagés avec chaque projet et groupe dans une instance GitLab. Lorsque des travaux sont exécutés sur des serveurs partagés, des calculer les minutes sont utilisées. Sur GitLab.com, le quota d'calculer les minutes est défini pour chaque <a href="https://docs.gitlab.com/ee/user/namespace/" data-ga-name="namespaces" data-ga-location="body">espace de noms</a>, et est déterminé par <a href="/pricing" data-ga-name="Your license tier" data-ga-location="body">votre niveau de licence.</a>.<p/>\n\n<p>En plus du quota mensuel, sur GitLab.com, vous pouvez <a href="https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#purchase-additional-cicd-minutes" data-ga-name="purchase additional compute minutes" data-ga-location="body">acheter des calculer les minutes supplémentaires</a> lorsque vous en avez besoin.</p>

          - title: Créez votre compte d'abonnement autogéré
            copies:
              - title: Déterminez le nombre de sièges que vous souhaitez
                text: |
                  Un abonnement autogéré GitLab utilise un modèle concurrent (siège). Vous payez un abonnement en fonction du nombre maximum d'utilisateurs pendant la période de facturation. Vous pouvez ajouter et supprimer des utilisateurs pendant la période d'abonnement, tant que le nombre total d'utilisateurs à un moment donné ne dépasse pas le nombre d'abonnements.
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/self_managed/#subscription-seats
                  text: Découvrez comment les sièges sont déterminés
                  ga_name: Determine how many seats you want
                  ga_location: body
              - title: Obtenir votre abonnement autogéré
                text: |
                  Vous pouvez installer, administrer et maintenir votre propre instance de GitLab. Accédez à la page de tarification et sélectionnez **Acheter Premium** ou **Acheter Ultimate**.
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/self_managed/
                  text: En savoir plus sur l'autogestion
                  ga_name: Learn more about self-managed
                  ga_location: body
              - title: Activer GitLab Enterprise Edition
                text: |
                  Lorsque vous installez une nouvelle instance de GitLab sans licence, seules les fonctionnalités gratuites sont activées. Pour activer plus de fonctionnalités dans GitLab Enterprise Edition (EE), activez votre instance avec le code d'activation fourni lors de l'achat. Le code d'activation se trouve dans l'e-mail de confirmation de l'achat ou dans le portail client sous Gérer les achats.
                link:
                  href: https://docs.gitlab.com/ee/user/admin_area/license.html
                  text: Détails de l'activation
                  ga_name: Activate GitLab Enterprise Edition
                  ga_location: body
              - title: Examiner les exigences du système
                text: |
                  <p>Passez en revue les systèmes d'exploitation pris en charge par <a href="https://docs.gitlab.com/ee/install/requirements.html" data-ga-name="system rqeuirements" data-ga-location="body">et la configuration minimale requise </a> pour installer et utiliser GitLab.</p>
              - title: Installieren Sie GitLab
                text: |
                  <p>Choisissez votre méthode d'installation <a href="https://docs.gitlab.com/ee/install/#choose-the-installation-method" data-ga-name="Installation Method" data-ga-location="body"></a></p>
                  <p>Installez sur <a href="https://docs.gitlab.com/ee/install/#install-gitlab-on-cloud-providers" data-ga-name="your cloud provider" data-ga-location="body">votre fournisseur de cloud</a> (si applicable)</p>
              - title: Configurez votre instance
                text: |
                  Cela inclut des choses comme la connexion de votre email à GitLab pour les notifications, la configuration du proxy de dépendance afin que vous puissiez mettre en cache les images de conteneurs à partir de Docker Hub pour des constructions plus rapides et plus fiables, et la détermination des exigences d'authentification, et plus encore.
                link:
                  href: https://docs.gitlab.com/ee/install/next_steps.html
                  text: Voyez ce que vous pouvez configurer
                  ga_name: Configure your self-managed instance
                  ga_location: body
              - title: Mise en place d'un environnement hors ligne (facultatif)
                text: |
                  "Mise en place d'un environnement hors ligne lorsqu'il est nécessaire de s'isoler de l'internet public (généralement applicable aux industries réglementées).
                link:
                  href: https://docs.gitlab.com/ee/user/application_security/offline_deployments/index.html
                  text: "L'environnement hors ligne vous convient-il?"
                  ga_name: Set up off-line environment
                  ga_location: body
              - title: Envisager de limiter le nombre de minutes autorisées pour les exécutants partagés CI/CD
                text: |
                  Pour contrôler l'utilisation des ressources sur les instances GitLab autogérées, le quota d'calculer les minutes pour chaque espace de noms peut être défini par les administrateurs.
                link:
                  href: https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#set-the-quota-of-cicd-minutes-for-a-specific-namespace
                  text: En savoir plus
                  ga_name: Consider limiting CI/CD shared runner minutes allowed
                  ga_location: body
              - title: Installer le runner GitLab
                text: |
                  GitLab Runner peut être installé et utilisé sur GNU/Linux, macOS, FreeBSD et Windows. Vous pouvez l'installer dans un conteneur, en téléchargeant un binaire manuellement ou en utilisant un dépôt de paquets rpm/deb.
                link:
                  href: https://docs.gitlab.com/runner/install/
                  text: Évaluer les options d'installation
                  ga_name: Install GitLab runner
                  ga_location: body
              - title: Configurer le runner GitLab (optionnel)
                text: |
                  GitLab Runner peut être configuré pour répondre à vos besoins et à vos politiques.
                link:
                  href: https://docs.gitlab.com/runner/configuration/
                  text: Voir les options de configuration de l'exécutant
                  ga_name: Configure GitLab runner
                  ga_location: body
              - title: Auto-administration
                text: |
                  "L'autogestion implique l'auto-administration. En tant qu'administrateur, vous pouvez adapter de nombreuses choses à vos besoins particuliers."
                link:
                  href: https://docs.gitlab.com/ee/administration/#configuring-gitlab
                  text: En savoir plus sur l'auto-administration
                  ga_name: Self Administration
                  ga_location: body
          - title: Intégrer des applications (en option)
            copies:
              - text: |
                  Vous pouvez ajouter des fonctionnalités telles que la gestion des secrets ou les services d'authentification, ou intégrer des applications existantes telles que les systèmes de suivi des problèmes.
                link:
                  href: https://docs.gitlab.com/ee/integration/
                  text: En savoir plus sur les intégration
                  ga_name: Integrate applications
                  ga_location: body
      - header: Utiliser GitLab
        show: Afficher tout
        hide: Cacher tout
        id: utiliser-gitlab
        items:
          - title: Mise en place de votre organisation
            copies:
              - text: |
                  Configurez votre organisation et ses utilisateurs. Déterminez les rôles des utilisateurs et donnez à chacun l'accès aux projets dont il a besoin.
                link:
                  href: https://docs.gitlab.com/ee/topics/set_up_organization.html
                  text: En savoir plus
                  ga_name: Setup your organization
                  ga_location: body
          - title: Organiser le travail en fonction des projets
            copies:
              - text: |
                  Dans GitLab, vous pouvez créer des projets pour héberger votre base de code. Vous pouvez également utiliser des projets pour suivre les problèmes, planifier le travail, collaborer sur le code et construire, tester et utiliser le système CI/CD intégré pour déployer votre application.
                link:
                  href: https://docs.gitlab.com/ee/user/project/index.html
                  text: En savoir plus
                  ga_name: Organize work with projects
                  ga_location: body
          - title: Planifier et suivre le travail
            copies:
              - text: |
                  Planifiez votre travail en créant des exigences, des questions et des thèmes. Planifiez le travail avec des étapes et suivez le temps passé par votre équipe. Apprenez à gagner du temps avec des actions rapides, voyez comment GitLab rend le texte Markdown, et apprenez à utiliser Git pour interagir avec GitLab.
                link:
                  href: https://docs.gitlab.com/ee/topics/plan_and_track.html
                  text: En savoir plus
                  ga_name: Plan and track work
                  ga_location: body
          - title: Créez votre application
            copies:
              - text: |
                  Ajoutez votre code source à un référentiel, créez des demandes de fusion pour vérifier le code et utilisez CI/CD pour générer votre application.
                link:
                  href: https://docs.gitlab.com/ee/topics/build_your_application.html
                  text: En savoir plus
                  ga_name: Build your application
                  ga_location: body
          - title: Sécurisez votre demande
            copies:
              - title: Déterminer les scanners que vous souhaitez utiliser
                text: |
                  GitLab propose la détection des secrets, SAST et l'analyse des conteneurs dans la version gratuite. DAST, Dependency and IaC scanning, API security, license compliance and fuzzing sont disponibles dans le niveau Ultimate. Tous les scanners sont activés par défaut. Vous pouvez choisir de les désactiver individuellement.
                link:
                  href: https://docs.gitlab.com/ee/user/application_security/configuration/
                  text: Configurer l'utilisation du scanner
                  ga_name: Determine which scanners to use
                  ga_location: body
              - title: Configurez vos politiques de sécurité
                text: |
                  Les politiques de GitLab permettent aux équipes de sécurité d'exiger que les analyses de leur choix soient exécutées chaque fois qu'un pipeline de projet s'exécute conformément à la configuration spécifiée. Les équipes de sécurité peuvent donc être sûres que les analyses qu'elles ont mises en place n'ont pas été modifiées, altérées ou désactivées. Des règles peuvent être définies pour l'exécution et les résultats de l'analyse.
                link:
                  href: https://docs.gitlab.com/ee/user/application_security/policies/
                  text: Configurer les politiques de sécurité
                  ga_name: Configure your security policies
                  ga_location: body
              - title: Configurer les règles d'approbation des demandes de fusion
                text: |
                  Vous pouvez configurer vos demandes de fusion de manière à ce qu'elles soient approuvées avant d'être fusionnées. Bien que GitLab Free permette à tous les utilisateurs ayant les permissions Developer ou plus d'approuver les demandes de fusion, ces approbations sont optionnelles. GitLab Premium et GitLab Ultimate offrent une flexibilité supplémentaire pour définir des contrôles plus granulaires. Les approbations MR par projet et au niveau du groupe. Les administrateurs des instances GitLab Premium et GitLab Ultimate autogérées peuvent également configurer les approbations pour l'ensemble de l'instance.
                link:
                  href: https://docs.gitlab.com/ee/user/project/merge_requests/approvals/
                  text: En savoir plus sur les règles d'approbation des RM
                  ga_name: Configure MR approval rules
                  ga_location: body
          - title: Déployer et publier votre application
            copies:
              - text: |
                  Déployez votre application en interne ou au public. Utiliser des drapeaux pour publier des fonctionnalités de manière incrémentale.
                link:
                  href: https://docs.gitlab.com/ee/topics/release_your_application.html
                  text: En savoir plus
                  ga_name: Deploy and release your application
                  ga_location: body
          - title: Contrôler les performances de l'application
            copies:
              - text: |
                  GitLab fournit une variété d'outils pour aider à opérer et maintenir vos applications.  Vous pouvez suivre les mesures les plus importantes pour votre équipe, générer des alertes automatiques en cas de dégradation des performances et gérer ces alertes, le tout dans GitLab.
                link:
                  href: https://docs.gitlab.com/ee/operations/index.html
                  text: En savoir plus
                  ga_name: Monitor application performance
                  ga_location: body
          - title: Contrôler les performances des coureurs
            copies:
              - text: |
                  GitLab dispose de son propre système de mesure de la performance des applications. GitLab Performance Monitoring permet de mesurer une grande variété de statistiques
                link:
                  href: https://docs.gitlab.com/runner/monitoring/index.html
                  text: En savoir plus
                  ga_name: Monitor runner performance
                  ga_location: body
          - title: Gérer votre infrastructure
            copies:
              - text: |
                  GitLab offre diverses fonctionnalités pour accélérer et simplifier vos pratiques de gestion de l'infrastructure.
                  * GitLab a des intégrations profondes avec Terraform pour l'approvisionnement de l'infrastructure en nuage qui vous aide à démarrer rapidement sans aucune configuration, à collaborer autour des changements d'infrastructure dans les demandes de fusion de la même manière que vous le feriez avec les changements de code, et à évoluer en utilisant un registre de modules.
                  * L'intégration de GitLab avec Kubernetes vous aide à installer, configurer, gérer, déployer et dépanner les applications en cluster.
                link:
                  href: https://docs.gitlab.com/ee/user/infrastructure/index.html
                  text: En savoir plus
                  ga_name: Manage your infrastructure
                  ga_location: body
          - title: Analyser l'utilisation de GitLab
            copies:
              - text: |
                  GitLab fournit des analyses au niveau du projet, du groupe et de l'instance. L'équipe DevOps Research and Assessment (DORA) a mis au point plusieurs mesures clés que vous pouvez utiliser comme indicateurs de performance pour les équipes de développement de logiciels. GitLab Ultimate les a inclus.
                link:
                  href: https://docs.gitlab.com/ee/user/analytics/index.html
                  text: En savoir plus
                  ga_name: Analyze GitLab usage
                  ga_location: body
  next_steps:
    header: Faites passer votre petite entreprise à l'étape suivante
    cards:
      - title: Avez-vous besoin d'une assistance à la clientèle?
        text: GitLab [documentation](https://docs.gitlab.com/){data-ga-name="documentation" data-ga-location="body"} peut répondre à vos questions.
        avatar: /nuxt-images/icons/avatar_orange.png
        col_size: 4
        link:
          text: Soutien à la clientèle
          url: /support/
          data_ga_name: Contact support
          data_ga_location: body
      - title: Besoin d'aide?
        text: Les services professionnels de GitLab peuvent vous aider à démarrer, à intégrer des applications tierces et à migrer depuis d'autres outils.
        avatar: /nuxt-images/icons/avatar_pink.png
        col_size: 4
        link:
          text: Que mon PS me contacte
          url: /sales/
          data_ga_name: Have my PS contact me
          data_ga_location: body
      - title: Vous préférez travailler avec un partenaire de distribution?
        text: Préférez-vous travailler avec un distributeur, un intégrateur ou un fournisseur de services gérés (MSP) ? Vous pouvez également acheter GitLab sur les places de marché de nos [partenaires cloud](/partners/technology-partners/){data-ga-name="cloud partners" data-ga-location="body"}.
        avatar: /nuxt-images/icons/avatar_blue.png
        col_size: 4
        link:
          text: Voir l'annuaire des partenaires de distribution
          url: https://partners.gitlab.com/English/directory/
          data_ga_name: See channel partner directory
          data_ga_location: body
      - title: Vous envisagez une mise à niveau?
        text: Découvrez les avantages de [Premium](/pricing/premium/){data-ga-name="why premium" data-ga-location="body"} et [Ultimate](/pricing/ultimate/){data-ga-name="why ultimate" data-ga-location="body"}.
        col_size: 6
        link:
          text: Pourquoi Ultimate
          url: /pricing/ultimate
          data_ga_name: Why ultimate
          data_ga_location: body
      - title: Vous envisagez l'intégration d'un tiers?
        text: Le noyau ouvert de GitLab facilite son intégration. Nous avons de nombreux partenaires technologiques qui complètent la plateforme DevSecOps complète de GitLab pour répondre aux besoins et aux cas d'utilisation uniques.
        col_size: 6
        link:
          text: Voir nos partenaires Alliance et Technologie
          url: /partners/technology-partners/
          data_ga_name: See our Alliance and Technology partners
          data_ga_location: body
