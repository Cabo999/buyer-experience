---
  title: Participer au programme GitLab pour l'éducation
  description: Grâce à GitLab pour l'éducation, le DevOps est à portée de main, directement dans votre salle de classe. Inscrivez-vous dès aujourd'hui pour commencer votre parcours DevOps !
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab pour l'éducation
        subtitle: Le DevOps à disposition sur votre campus
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: '#application'
          text: Participer au programme GitLab pour l'éducation
          data_ga_name: join education program
          data_ga_location: header
          icon:
            name: arrow-down
            variant: product
        image:
          image_url: /nuxt-images/solutions/education.jpeg
          alt: "Image\_: GitLab pour l'éducation"
          rounded: true
    - name: tabs-menu
      data:
        column_size: 8
        menus:
          - id_tag: requirements
            text: Exigences
            data_ga_name: requirements
            data_ga_location: header
          - id_tag: application
            text: Inscription
            data_ga_name: application
            data_ga_location: header
          - id_tag: renewal
            text: Renouvellement
            data_ga_name: renewal
            data_ga_location: header
          - id_tag: frequently-asked-questions
            text: FAQ
            data_ga_name: frequently asked questions
            data_ga_location: header
    - name: copy-media
      data:
        block:
          - header: Exigences
            aos_animation: fade-up
            aos_duration: 500
            hide_horizontal_rule: true
            metadata:
              id_tag: exigences
            miscellaneous: |
              ##### In order to be accepted into the GitLab for Education Program, each educational institution must meet the following requirements.

              * **Accrédité :** l'établissement d'enseignement doit être accrédité par une autorité locale, étatique, provinciale, fédérale ou nationale agréée. [En savoir plus]((/handbook/marketing/community-relations/community-programs/education-program/#gitlab-for-education-program-requirements){data-ga-name="accredited" data-ga-location="body"}.
              * **Objectif principal de l'enseignement :** l'établissement d'enseignement doit avoir comme objectif principal l'apprentissage de ses étudiants inscrits.
              * **Délivrance de diplômes :** l'établissement d'enseignement doit délivrer activement des diplômes validant avec succès un cursus d'études supérieures (licence-master-doctorat ou équivalent).
              * **À but non lucratif :** l'établissement d'enseignement doit s'engager sur le caractère non lucratif de son activité. Les entités à but lucratif ne sont pas éligibles.

              ##### GitLab for Education Licenses can only be used for

              * **Utilisation pédagogique :** activités directement liées à l'apprentissage, à la formation ou au développement des étudiants, notamment l'enseignement académique qui fait partie des fonctions pédagogiques de l'établissement d'enseignement ou
               * **Recherche académique à finalité non commerciale :** projets de recherche à but non lucratif qui ne produisent pas de résultats, de travaux, de services ou de données destinés à une utilisation commerciale par quiconque dans le but de générer des revenus. Les recherches menées à la demande et au profit d'un tiers ne sont pas autorisées dans le cadre de la licence GitLab pour l'éducation.
              * **Gérer, administrer ou faire fonctionner un établissement au moyen de la licence GitLab pour l'éducation n'est pas autorisé.** GitLab propose des réductions ainsi que des tarifs spéciaux pour les universités et autres établissements d'enseignement pour une utilisation sur l'ensemble du campus. [En savoir plus](/solutions/education/campus/){data-ga-name= "campus pricing" data-ga-location="body"}.

              * **Remarque :** pour le moment, les établissements qui accueillent des élèves âgés de moins de 13 ans ne sont pas éligibles à l'abonnement GitLab SaaS. En revanche, ils peuvent utiliser une licence GitLab Auto-géré.

              ##### Applicants must

              * **Enseignants ou personnel :** seuls les enseignants ou le personnel travaillant à temps plein dans un établissement d'enseignement peuvent s'inscrire au programme. Nous ne sommes pas en mesure de délivrer des licences directement aux étudiants.
              * **Domaine de messagerie professionnelle :** les candidats au programme GitLab pour l'éducation doivent s'inscrire avec l'adresse de courriel délivrée par leur établissement. Les domaines de messagerie personnels ne seront pas acceptés.

              ##### Country of Origin

              * GitLab, Inc. ne délivre pas de licences aux établissements d'enseignement situés en Chine. Pour de plus amples informations sur l'obtention en Chine d'une licence Éducation, veuillez [contacter JiHu](mailto:ychen@gitlab.cn). [En savoir plus sur JiHu](/blog/2021/03/18/gitlab-licensed-technology-to-new-independent-chinese-company/){data-ga-name="more about JiHu" data-ga-location="body"}.

              #### GitLab for Education Agreement

              *  Dès que leur candidature est acceptée, tous les membres du programme sont soumis à l'[accord du programme GitLab pour l'éducation](/handbook/legal/education-agreement/){data-ga-name="education agreement" data-ga-location="body"}.

              #### Program Benefits

              * Nombre illimité de sièges par licence de nos fonctionnalités principales (GitLab SaaS ou GitLab Auto-géré). Le nombre de sièges correspond au nombre d'utilisateurs différents qui utiliseront cette licence au cours de l'année à venir.
              * Le nombre de sièges et le type de licence (GitLab SaaS ou GitLab Auto-géré) peuvent être ajustés au moment du renouvellement ou sur demande.
              * Le service d'assistance de GitLab n'est pas inclus dans la licence Éducation.
              * 50 000 minutes de runner CI sont incluses dans l'abonnement. ([Il est possible d'acheter des minutes supplémentaires](https://docs.gitlab.com/ee/subscriptions/#purchasing-additional-ci-minutes){data-ga-name="additional minutes" data-ga-location="body"}).
    - name: copy-form
      data:
        form:
          external_form:
            url: https://offers.sheerid.com/gitlab/university/teacher/
            width: 800
            height: 1300
        header: Inscription
        metadata:
          id_tag: inscription
        form_header: Formulaire d'inscription au programme
        datalayer: sales

        content: |
          ## Processus de demande
          * Remplissez le formulaire d'inscription ci-contre. Veuillez fournir les informations les plus précises et les plus complètes possible.
          * GitLab utilise SheerID, un partenaire de confiance, pour vérifier que vous exercez le métier d'enseignant, professeur ou que vous êtes un membre du personnel d'un établissement d'enseignement qualifié.

          ## À quoi s'attendre
          Après avoir rempli le formulaire d'inscription et si vous avez passé l'étape de vérification de votre fonction, vous recevrez un courriel de confirmation contenant des instructions pour télécharger votre licence. Veuillez suivre attentivement ces instructions.

          ## Aide et soutien
          Si vous rencontrez des problèmes pour télécharger votre licence sur le portail client, veuillez ouvrir un ticket d'assistance sur le [portail d'assistance GitLab](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293) et sélectionnez « Problèmes relatifs aux licences et aux renouvellements ».
    - name: copy
      data:
        aos_animation: fade-up
        aos_duration: 500
        block:
          - header: Renouvellement
            metadata:
              id_tag: renouvellement
            subtitle: Demande de renouvellement
            column_size: 10
            text: |
              Les licences GitLab pour l'éducation doivent être renouvelées chaque année. Les exigences du programme peuvent changer de temps à autre, et nous devrons nous assurer que les membres qui renouvellent leur licence continuent à les respecter.

               Avant de faire une demande de renouvellement, veuillez :

              * vérifier vos autorisations. La personne qui demande le renouvellement de l'abonnement doit être la même que celle qui a créé l'abonnement dans le portail client de GitLab pour l'établissement concerné.
              * Si vous souhaitez qu'une autre personne effectue la demande de renouvellement, le propriétaire actuel doit [transférer la propriété du compte du portail clients](https://docs.gitlab.com/ee/subscriptions/#change-account-owner-information). Si le propriétaire actuel n'est plus en mesure de transférer la propriété ou de renouveler son abonnement, veuillez [ouvrir un ticket d'assistance](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293) pour changer le propriétaire de l'abonnement.


              Qu'il s'agisse d'une première demande d'inscription au programme ou du renouvellement de votre abonnement dans le cadre de ce programme, vous devez remplir le même formulaire.

              Une fois le formulaire rempli et envoyé, vous devrez passer l'étape de vérification. Une fois votre statut vérifié, vous recevrez un courriel de confirmation contenant des instructions pour pouvoir télécharger votre licence. Veuillez suivre attentivement les instructions.
             
             
    - name: faq
      data:
        metadata:
          id_tag: frequently-asked-questions
        aos_animation: fade-up
        aos_duration: 500
        header: FAQ
        groups:
          -
            questions:
              - question: Les laboratoires et instituts de recherche sont-ils admissibles à la licence Éducation ?
                answer: |
                  Oui, les activités de recherche sont éligibles si l'établissement d'enseignement concerné est éligible et si ces activités relèvent de la recherche académique à but non commercial. Veuillez consulter les exigences de notre programme pour en savoir plus.
              - question: Est-il possible d'exécuter plusieurs instances de GitLab Auto-géré en utilisant la même clé de licence ?
                answer: |
                  Oui. Il est possible d'activer plusieurs instances de GitLab Auto-géré en utilisant la même clé de licence.
              - question: Pourquoi GitLab ne fournit-il pas de licences gratuites directement aux étudiants ?
                answer: Notre programme GitLab pour l'éducation est destiné directement à l'établissement d'enseignement (en tant qu'entreprise) plutôt qu'aux particuliers. Nous offrons un nombre illimité de sièges et de licences donnant accès aux meilleures fonctionnalités de GitLab, afin que tous les étudiants d'un établissement d'enseignement puissent accéder à l'ensemble des avantages qu'offre GitLab. Nous comprenons que les étudiants à titre personnel peuvent souhaiter posséder également une licence GitLab, mais pour le moment, la logistique en place ne nous permet pas d'accorder des licences individuelles. Nous encourageons tous les étudiants à trouver un maître d'études parmi les enseignants ou les membres du personnel de leur établissement afin que celui-ci rejoigne le programme. Les étudiants peuvent également se tourner vers notre abonnement gratuit à GitLab.com ou télécharger notre offre gratuite de GitLab Auto-géré. Il est aussi possible de bénéficier d'un essai gratuit pendant 30 jours pour pouvoir tester des fonctionnalités plus avancées.
              - question: Comment gérer la visibilité des projets ?
                answer: |
                  Si vous êtes membre du groupe parent dans GitLab, vous aurez automatiquement accès à tous les descendants. GitLab ne prend pas en charge les sous-groupes qui sont plus restrictifs que leurs groupes parents. Cependant, faire partie d'un sous-groupe ne vous donne pas accès au groupe parent.
                  \
                  La meilleure façon de procéder est de faire de sorte que chacun soit membre de son sous-groupe respectif et que seuls les administrateurs fassent partie du groupe principal.
              - question: Cette licence peut-elle être utilisée dans le service informatique pour gérer des services informatiques ?
                answer: |
                  Non, l'utilisation de la licence par l'équipe du service informatique de l'établissement ou toute utilisation administrative servant à la gestion de l'établissement n'est pas autorisée. La licence GitLab Éducation ne peut être utilisée qu'à des fins d'enseignement ou de recherche. Veuillez contacter notre équipe commerciale si vous souhaitez utiliser GitLab pour un usage professionnel au sein d'un service informatique.
              - question: Les étudiants peuvent-ils utiliser notre instance GitLab après avoir obtenu leur diplôme ?
                answer: |
                  Les licences GitLab Éducation sont délivrées directement à l'établissement d'enseignement. Par conséquent, les étudiants devront acheter leur propre licence s'ils ne sont plus autorisés à y accéder par l'intermédiaire de leur établissement.
              - question: Est-il autorisé de modifier le contrat de licence de l'utilisateur final ?
                answer: |
                  Pour le moment, nous ne sommes pas en mesure d'accepter des modifications apportées à notre contrat d'utilisation. Veuillez envoyer un courriel à l'adresse education@gitlab.com si vous avez des questions à ce sujet.
              - question: Est-il possible d'authentifier les utilisateurs via LDAP sur SSL ?
                answer: |
                  Cela n'est possible que sur notre version GitLab Auto-géré. Le serveur n'a pas strictement besoin d'une adresse IP statique, car un nom DNS peut être utilisé pour le serveur LDAP.
              - question: Est-il possible d'augmenter le nombre de sièges par la suite ?
                answer: |
                  Si vous souhaitez augmenter le nombre de sièges de votre licence actuelle, veuillez envoyer un courriel à l'adresse education@gitlab.com, et nous vous établirons un devis pour les sièges supplémentaires.
              - question: Qui est comptabilisé dans l'abonnement ?
                answer: |
                  Veuillez consulter notre FAQ dédiée aux licences et aux abonnements pour en savoir plus.
              - question: À qui s'adresser pour obtenir de l'aide ?
                answer: |
                   Veuillez consulter la rubrique [Aide pour les programmes communautaires](https://docs.gitlab.com/ee/subscriptions/#support-for-community-programs) pour trouver de l'aide. Veuillez noter qu'il n'est plus possible d'acheter un service d'assistance distinct pour les licences GitLab pour l'éducation. En revanche, les établissements d'enseignement éligibles ont la possibilité d'acheter l'[abonnement GitLab pour les campus](/solutions/education/campus/).