---
  title: Gestion des chaînes de valeur
  description: La gestion des chaînes de valeur de GitLab fournit une méthode systématique pour réduire le temps de valorisation, optimiser les résultats commerciaux et améliorer la qualité des logiciels.
  report_cta:
    layout: "dark"
    title: Rapports des analystes
    reports:
    - description: "GitLab reconnu comme unique Leader dans le rapport The Forrester Wave™, catégorie Plateformes intégrées de livraison de logiciels (2e\_trimestre\_2023)"
      link_text: Lire le rapport
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
    - description: "GitLab reconnu Leader dans le Gartner® Magic Quadrant™ 2023 pour les plateformes DevOps"
      link_text: Lire le rapport
      url: /gartner-magic-quadrant/
  components:
    - name: 'solutions-hero'
      data:
        title: Gestion des chaînes de valeur
        subtitle: Mesurer et gérer la valeur commerciale de votre cycle de vie DevSecOps.
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Commencer votre essai gratuit
          url: /free-trial/
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Image\_: gitlab + gestion des chaînes de valeur"
    - name: copy-media
      data:
        block:
          - header: Avantages de la gestion des chaînes de valeur
            miscellaneous: |
              Le développement logiciel devrait toujours viser à maximiser la livraison de la valeur client ou l'entreprise. Cependant, comment identifiez-vous les inefficacités dans cette livraison ? Et comment corriger le tir dans ce cas ? La gestion des chaînes de valeur de GitLab aide les entreprises à visualiser leur flux de travail DevSecOps de bout en bout, à identifier et à cibler le gaspillage et les inefficacités ainsi qu'à prendre des mesures pour optimiser ces flux de travail afin de fournir la vitesse de valeur la plus élevée possible.
          - header: Une plateforme de livraison de chaînes de valeur
            miscellaneous: |
              Dans sont article, [The Future of DevOps Toolchains Will Involve Maximizing Flow in IT Value Streams (L'avenir des chaînes d'outils DevOps passe par la maximisation des chaînes de valeur informatiques)](https://www.gartner.com/en/documents/3979558/the-future-of-devops-toolchains-will-involve-maximizing){data-ga-name="The Future of DevOps Toolchains Will Involve Maximizing Flow in IT Value Streams" data-ga-location="body"}, Gartner recommandait que « les leaders de l'infrastructure et des opérations responsables de la sélection et du déploiement des chaînes d'outils DevOps devraient stimuler la capacité commerciale en utilisant des plateformes de livraison de chaînes de valeur DevOps qui réduisent la charge de gestion des chaînes d'outils complexes. » [1] En fournissant une [plateforme DevOps complète](/solutions/devops-platform/){data-ga-name="GitLab - The DevOps Platform" data-ga-location="body"} en tant qu'application unique, GitLab est particulièrement adaptée pour fournir une visibilité de bout en bout tout au long du cycle de vie sans la « taxe sur la chaîne d'outils ». En tant que lieu de travail, GitLab peut également associer la visualisation à l'action, permettant ainsi aux utilisateurs de passer de l'apprentissage à la pratique à tout moment, sans perdre de contexte.

              *[1] Gartner « The Future of DevOps Toolchains Will Involve Maximizing Flow in IT Value Streams », Manjunath Bhat, et al, 14 janvier 2020 (abonnement à Gartner requis)*
            media_link_href: https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html
            media_link_text: Analyse de chaîne de valeur
            media_link_data_ga_name: Value Stream Analytics
            media_link_data_ga_location: body
          - header: Visualiser et gérer les processus de bout en bout
            text: |
              L'analyse de la chaîne de valeur vous aide à visualiser et à gérer le flux DevSecOps, de l'idéation à la livraison à la clientèle. Dès sa sortie, GitLab a commencé à offrir des rapports exploitables sur les flux de travail et les métriques courants, sans que rien ne doive être installé ou configuré. Si vous souhaitez approfondir ou modéliser des flux de travail personnalisés, le magasin de données unifié et complet de GitLab facilite le suivi des événements importants.

              *   **Un seul outil, pas de chaîne :** GitLab unifie l'ensemble de la chaîne d'outils DevSecOps en une seule application. Il n'y a pas d'intégrations à gérer, pas de goulots d'étranglement API pour limiter la visibilité. Mais, il y a une expérience partagée pour tout le monde dans l'entreprise, quel que soit son rôle.

              *   **Données partagées et exploitables :** une source unique d'insights basée sur un seul système de travail signifie que vous pouvez passer plus de temps à développer de la valeur et moins de temps à trouver où elle est bloquée.

              *   **Focus sur la valeur, pas sur la forme :** suivez et gérez le flux de travail réel et éliminez tout ce qui est inutile. Une seule application DevSecOps signifie un simple clic sur les éléments de travail réels. Ainsi, vous pouvez supprimer les blocages dès que vous les trouvez.

            video:
              video_url: https://www.youtube.com/embed/8pLEucNUlWI
            inverted: true
          - header: Mesure
            text: |
              Favorisez l'amélioration continue en vous basant sur les données de votre chaîne de valeur.

              * **Suivi du flux et accélération :** se concentrer sur le flux de valeur pour les clients est essentiel pour rationaliser le processus de livraison.
              * **Temps de cycle :** suivez le travail et les efforts réels, le temps de cycle réel pour générer de la valeur.
              * **Valeur commerciale :** reliez la valeur réelle à la modification.
              * **Métriques DORA4 :** les [métriques DORA4](https://about.gitlab.com/solutions/value-stream-management/dora/) vous aident à comparer votre benchmark DevSecOps et constituent un indicateur utile pour la comparaison au sein de l'équipe ou d'entreprises/industries comparables. Surveillez la [durée d'exécution de la modification](https://docs.gitlab.com/ee/api/dora4_project_analytics.html#list-project-merge-request-lead-times){data-ga-name="DORA4 Analytics Project API | GitLab" data-ga-location="body"} et la [fréquence de déploiement](https://docs.gitlab.com/ee/api/dora4_project_analytics.html){data-ga-name="DORA4 Analytics Project API | GitLab" data-ga-location="body"} pour mesurer l'efficacité de vos processus DevSecOps.
            image:
              image_url: /nuxt-images/solutions/tasks-by-type.png
              alt: ""
              caption: |
                [Voir la répartition de la valeur commerciale entre les types de travail pour mieux aligner l'exécution sur la stratégie.](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html#type-of-work---tasks-by-type-chart){data-ga-name="Value Stream Analytics | GitLab" data-ga-location="body"}
          - header: Gestion
            text: |
              Les chaînes de valeur vous aident à visualiser et à gérer le flux de nouvelles innovations, de l'idée à la clientèle.

              * **Identifier les contraintes :** trouvez les sources de friction dans votre chaîne de valeur et optimisez la durée d'exécution.
              * **Supprimer les goulots d'étranglement :** prenez des mesures pour résoudre les contraintes.
              * **Optimiser le flux :** accélérez la mise en place des meilleures pratiques DevSecOps dans votre organisation.
              * **Gérer l'efficacité :** les [insights détaillés](https://docs.gitlab.com/ee/user/project/insights/){data-ga-name="Value Stream Analytics | GitLab" data-ga-location="body"} présentant votre hygiène de triage, les tickets/bugs créés, le temps de fusion et bien d'autres vous permettent d'identifier et de corriger facilement les inefficacités de votre processus de développement.
            image:
              image_url: /nuxt-images/solutions/days-to-complete.png
              alt: ""
              caption: |
                [Inspecter et améliorer les processus, repérer les tendances et cherchez les causes profondes des valeurs aberrantes.](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html#days-to-completion-chart){data-ga-name="Insights | GitLab" data-ga-location="body"}
              vertical_centered: true
            inverted: true
    - name: 'solutions-carousel-videos'
      data:
        title: Gestion des chaînes de valeur
        videos:
          - title: "Insights pour les cadres GCV"
            photourl: /nuxt-images/video-carousels/vsm_1/1.jpg
            video_link: https://www.youtube-nocookie.com/embed/0MR7Q8Wiq6g
            carousel_identifier:
              - ''

          - title: "Insights pour les développeurs GCV"
            photourl: /nuxt-images/video-carousels/vsm_1/4.jpg
            video_link: https://www.youtube-nocookie.com/embed/JPYWrRByAYk
            carousel_identifier:
              - ''

          - title: "Insights sur la sécurité GCV"
            photourl: /nuxt-images/video-carousels/vsm_1/5.jpg
            video_link: https://www.youtube-nocookie.com/embed/KSJ9VhKQAS0
            carousel_identifier:
              - ''

          - title: "Insights sur les opérations GCV"
            photourl: /nuxt-images/video-carousels/vsm_1/4.jpg
            video_link: https://www.youtube-nocookie.com/embed/lDWxH2YO3Yk
            carousel_identifier:
              - ''

          - title: "Insights sur la productivité GCV"
            photourl: /nuxt-images/video-carousels/vsm_1/5.jpg
            video_link: https://www.youtube-nocookie.com/embed/vYKC7r8ztVA
            carousel_identifier:
              - ''

          - title: "Tableaux des tickets GCV pour la correspondance"
            photourl: /nuxt-images/video-carousels/vsm_1/6.jpg
            video_link: https://www.youtube-nocookie.com/embed/9ASHiQ2juYY
            carousel_identifier:
              - ''

          - title: "Surveillance de la valeur commerciale GCV"
            photourl: /nuxt-images/video-carousels/vsm_1/7.jpg
            video_link: https://www.youtube-nocookie.com/embed/oG0VESUOFAI
            carousel_identifier:
              - ''
