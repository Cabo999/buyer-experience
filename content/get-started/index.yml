  title: Get started with GitLab
  description: How would you like to get started?
  components:
    - name: 'solutions-hero'
      data:
        title: Get Started with GitLab
        subtitle: New to GitLab and not sure where to start? We’ll walk you through the basics so you know what to expect along the way.
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Start your free trial
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: header
        secondary_btn:
          url: /demo/
          text: Watch a demo
          data_ga_name: demo
          data_ga_location: header
          variant: tertiary
          icon:
            name: play
            variant: product
        image:
          image_url: /nuxt-images/get-started/working-person.jpeg
          alt:  "Image: a couple of hands working on a laptop"
          bordered: true
    - name: get-started-menu
      data:
        title: Quick setup checklists
        block:
          - name: Put your business first
            links:
              - text: For Small Business
                href: /get-started/small-business/
                data_ga_name: for small business
                data_ga_location: body
                icon:
                  name: clipboard-checks
                  alt: Clipboard Icon
              - text: For Enterprise
                href: /get-started/enterprise/
                data_ga_name: for enterprise
                data_ga_location: body
                icon:
                  name: clipboard-checks
                  alt: Clipboard Icon
              - text: For building your business case
                href: /get-started/build-business-case/
                data_ga_name: for building your business case
                data_ga_location: body
                icon:
                  name: clipboard-checks
                  alt: Clipboard Icon
              - text: Why GitLab
                href: /why-gitlab/
                data_ga_name: simplify software development with The One DevOps Platform
                data_ga_location: body
                icon:
                  name: docs-alt
                  alt: Docs Icon
          - name: Get the most from GitLab
            links:
              - text: For scaling your GitLab usage
                href: https://docs.gitlab.com/ee/development/scalability
                data_ga_name: for scaling your gitlab usage
                data_ga_location: body
                icon:
                  name: increase
                  alt: Increase Icon
              - text: Get Started with Continuous Integration
                href: /get-started/continuous-integration/
                data_ga_name: get started with continuous integration
                data_ga_location: body
                icon:
                  name: cog
                  alt: cog Icon
              - text: For setting up security and compliance
                href: /solutions/continuous-software-security-assurance/
                data_ga_name: for setting up security and compliance
                data_ga_location: body
                icon:
                  name: cog-code
                  alt: Code cog Icon
              - text: For installing newer versions
                href: https://docs.gitlab.com/ee/update/
                data_ga_name: for installing newer versions
                data_ga_location: body
                icon:
                  name: cog-check
                  alt: Check cog Icon
              - text: For up-tiering
                href: /pricing/
                data_ga_name: for up-tiering
                data_ga_location: body
                icon:
                  name: increase
                  alt: Increase Icon
              - text: For using more capabilities
                href: /features/
                data_ga_name: for using more capabilities
                data_ga_location: body
                icon:
                  name: cogs
                  alt: Cogs Icon
          - name: Make a seamless transition
            links:
              - text: For transitioning from GitHub
                href: https://docs.gitlab.com/ee/user/project/import/github.html
                data_ga_name: for transitioning from github
                data_ga_location: body
                icon:
                  name: digital-transformation
                  alt: Digital transformation Icon
              - text: For importing your project issues from Jira
                href: https://docs.gitlab.com/ee/user/project/import/jira.html
                data_ga_name: for importing your project issues from jira
                data_ga_location: body
                icon:
                  name: digital-transformation
                  alt: Digital transformation Icon
              - text: For migrating other projects to GitLab
                href: https://docs.gitlab.com/ee/user/project/import/
                data_ga_name: for migrating other projects to gitlab
                data_ga_location: body
                icon:
                  name: digital-transformation
                  alt: Digital transformation Icon
    - name: get-started-resources
      data:
        title: More Resources
        cards:
          - title: GitLab Docs
            icon:
              name: docs-alt
              alt: Gitlab Docs Icon
            description: Documentation for GitLab Community Edition, GitLab Enterprise Edition, Omnibus GitLab, and GitLab Runner.
            link:
              text: Visit Docs
              url:  https://docs.gitlab.com/
          - title: Developer Portal
            icon:
              name: code
              alt: Developer Portal Icon
            description: Documentation for contributors to the GitLab Project -- information about our codebase, APIs, webhooks, design system, UI framework, and more!
            link:
              text: See Developer Portal
              url: https://developer.gitlab.com/
          - title: Blog
            icon:
              name: doc-pencil-alt
              alt: Blog Icon
            description: Visit the GitLab blog to learn about releases, applications, contributions, news, events, and more.
            link:
              text: Read the Blog
              url: /blog/
